import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/splash.dart';
// import 'package:flutter_jamu_tujuh/views/pages/login.dart';
// import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'dart:async';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
    home: OPSplashScreen(),
    debugShowCheckedModeBanner: false,
  ));
}
