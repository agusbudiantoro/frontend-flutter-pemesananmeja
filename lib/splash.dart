import 'dart:async';
import 'dart:convert';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/admin/home/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/home/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
// import 'package:flutter_jamu_tujuh/views/admin/pages/menu.dart';
// import 'package:flutter_jamu_tujuh/views/pages/login.dart';
// import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'views/pages/login/main.dart';

// import 'views/pages/menu.dart';

class OPSplashScreen extends StatefulWidget {
  final bool tandaUpdate;
  static String tag = '/OPSplashScreen';

  OPSplashScreen({this.tandaUpdate});
  @override
  _OPSplashScreenState createState() => _OPSplashScreenState();
}

class _OPSplashScreenState extends State<OPSplashScreen>
    with SingleTickerProviderStateMixin {
  String kode;
  String tokens;
  bool loginStatus;
  bool statusTanda;
  BlocLoginBloc bloc = BlocLoginBloc();
  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohome);
  }

  startTimeUser() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statustohomeUser);
  }

  startTimeLogin() async {
    var _duration = Duration(seconds: 3);
    return Timer(_duration, statuslogin);
  }

  @override
  void initState() {
    super.initState();
    bloc..add(BlocCekToken());
      
  }

  void navigationPage()async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Login(),
      ),
    );
  }

  Future<String> statuslogin() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Login(),
      ),
    );
  }

  Future<String> statustohome() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomeAdmin(),
      ),
    );
  }

  Future<String> statustohomeUser() async {
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomeUser(),
      ),
    );
  }

  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        body: BlocListener<BlocLoginBloc, BlocLoginState>(
          bloc: bloc,
          listener: (context, state) {
            // TODO: implement listener
            if(state is BlocaStateCekTokenFailed){
              startTimeLogin();
            }
            if(state is BlocStateCekTokenSukses){
              if(state.data.role == 1){
                startTime(); //jika role admin maka ke page admin
              } else {
                startTimeUser(); //jika role user maka ke page user
              }
            }
          },
          child: Container(
            decoration: BoxDecoration(
              // image: DecorationImage(
              //   image: AssetImage("assets/splash/splash.jpg"),
              //   fit: BoxFit.cover,
              // ),
            ),
            child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
              bloc:bloc,
              builder: (context, state) {
                if(state is BlocStateCekTokenSukses){
                  return myBody(size);
                }
                if(state is BlocStateLoading){
                  return myBody(size);
                }
                if(state is BlocaStateCekTokenFailed){
                  return myBody(size);
                }
                return myBody(size);
              },
            ),
          ),
        )
    );
  }

  Container myBody(Size size) {
    return Container(
      width: size.width,
      height: size.height,
      // color: background1,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.transparent, background1],
              begin: Alignment.topRight,
              end: Alignment.bottomRight)),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Image.asset("assets/logo/logo.png", width: 200,),
          Text(
                "Booking",
                style: TextStyle(
                    fontSize: 30,
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold,
                    color: circlePurpleDark),
              ),
              Container(
                margin: EdgeInsets.only(left:60),
                child: Text("Cafe",
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'RobotoCondensed',
                        color: circlePurpleDark)),
              ),
          SizedBox(
            height: 0,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              
            ],
          )
        ],
      ),
    );
  }
}
