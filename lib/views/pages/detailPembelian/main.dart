import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanan_bloc/pesanan_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/historyPembelian/historyPembelian.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/metodePengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/rincianHarga.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/alamatPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/detailbarang.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/historyStatusPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/informasiPesanan.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/link_pembayaran.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/noPesanan.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/noTransaksi.dart';

class PageDetailPembelian extends StatefulWidget {
  final ValueGetPemesanan dataPesanan;
  PageDetailPembelian({this.dataPesanan});
  @override
  _PageDetailPembelianState createState() => _PageDetailPembelianState();
}

class _PageDetailPembelianState extends State<PageDetailPembelian> {
  PesananBloc blocPesan = PesananBloc();
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: captionColor,
      appBar: AppBar(
        elevation:0,
        backgroundColor: contentColor,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: colorGrey,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Detail Pembelian",
                    style: TextStyle(fontSize: 14, color: colorGrey),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.grey[350],
        child: ListView(
          children: [
            SizedBox(
              height: 10,
            ),
            PageNoPesanan(noPesanan: widget.dataPesanan.noRiwayatPesanan.toString(),),
            SizedBox(
              height: 10,
            ),
            WidgetInformasiPesanan(width: size.width,height: size.height, status: widget.dataPesanan.status,statusCode: widget.dataPesanan.statusCodePembayaran,waktuPembayaran: widget.dataPesanan.waktuPembayaran,namaPembayaran: widget.dataPesanan.namaMetodePembayaran,logoPembaayaran: widget.dataPesanan.logoMetodePembayaran,totalHarga: widget.dataPesanan.totalHarga.toString(),),
            SizedBox(
              height: 10,
            ),
            WidgetLink(width: size.width,height: size.height,link: widget.dataPesanan.link_pembayaran.toString(),),
            SizedBox(
              height: 10,
            ),
            WidgetAlamatPem(width: size.width,height: size.height,alamat: widget.dataPesanan.alamat.toString(),),
            SizedBox(
              height: 10,
            ),
            PageNoTransaksi(noTransaksi: widget.dataPesanan.midtransIdPemesanan,width: size.width,),
            WidgetHistoryPengiriman(width: size.width,height: size.height,statusPengiriman: widget.dataPesanan.statusPengiriman,),
            WidgetDaftarBelanjaPembelian(height: size.height,width: size.width,dataBarang: BarangModel(stok: widget.dataPesanan.qty, qty: widget.dataPesanan.qty,totalHarga: widget.dataPesanan.totalHarga, harga: widget.dataPesanan.harga.toString(),hargaPromo: widget.dataPesanan.harga.toString(),namaBarang: widget.dataPesanan.namaBarang,image: widget.dataPesanan.image,),),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(8),
        alignment: Alignment.centerLeft,
        height: size.height/12,
        color: Colors.white,
        child: Column(
          children: [
            (widget.dataPesanan.statusPengiriman == 2)?BlocListener<PesananBloc, PesananState>(
                  bloc:blocPesan,
                  listener: (context, state) {
                    // TODO: implement listener
                    if(state is StatePutStatusPesananSukses){
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>BackgroundHistori()));
                    }
                  },
                  child: Container(
                    child: BlocBuilder<PesananBloc, PesananState>(
                      bloc:blocPesan,
                      builder: (context, state) {
                        if(state is StatePutStatusPesananSukses){
                          return buttonAmbil(size);
                        }
                        if(state is StatePutStatusPesananWaiting){
                          return Center(child: CircularProgressIndicator(backgroundColor: background1,color: captionColor,));
                        }
                        if(state is StatePutStatusFailed){
                          return buttonAmbil(size);
                        }
                        return buttonAmbil(size);
                      },
                    ),
                  ),
                ):Container()
          ],
        ),
      ),
    );
  }

  Container buttonAmbil(Size size) {
    return Container(
            width: size.width,
            child: ElevatedButton(
              onPressed: (){
                return blocPesan..add(EventPutStatusPesanan(data: ValueGetPemesanan(noRiwayatPesanan: widget.dataPesanan.noRiwayatPesanan,statusPengiriman: widget.dataPesanan.statusPengiriman)));
              },
              child: Text("Klaim Barang", style: TextStyle(color: Colors.white),),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(captionColor)
              ),
            )

          );
  }
}
