import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/login/model_form_login.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_textField.dart';

class PageGantiPassword extends StatefulWidget {

  @override
  _PageGantiPasswordState createState() => _PageGantiPasswordState();
}

class _PageGantiPasswordState extends State<PageGantiPassword> {
  bool statusPass = true;
  bool statusPass1 = true;
  bool statusPass2 = true;
  bool statusButton = false;
  final TextEditingController _passWord = TextEditingController();
  final TextEditingController _passWordBaru = TextEditingController();
  final TextEditingController _passWordBaruValid = TextEditingController();
  BlocLoginBloc blocPass = BlocLoginBloc();

  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  void _clickCallBackNew() {
    setState(() {
      statusPass1 = !statusPass1;
    });
  }

  void _clickCallBackNewValid() {
    setState(() {
      statusPass1 = !statusPass1;
    });
  }

  void _onChange() {
    setState(() {
      // statusPass1 = !statusPass1;
      if(_passWordBaruValid.text.toString() == _passWordBaru.text.toString()){
        statusButton = true;
      } else {
        statusButton = false;
      }
    });
  }

  void tampilSnack()async{
    await ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Password Berhasil Diubah"),
      duration: Duration(milliseconds: 500),
      onVisible: (){
        Navigator.pop(context);
      },
      backgroundColor: Colors.green,
    ));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: colorGrey,
      appBar: AppBar(
        backgroundColor: captionColor,
        title: Text("Ganti Password"),
      ),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Container(
              color: colorGrey,
              child: TextFieldLogin(
                  typePass: true,
                  typeInput: false,
                  isiField: _passWord,
                  hintText: "Password Lama",
                  prefixIcon: Icons.vpn_key_rounded,
                  isObsercure: statusPass,
                  suffixIcon: Icons.remove_red_eye,
                  clickCallback: () => _clickCallBack()),
            ),
            Container(
              color: colorGrey,
              child: TextFieldLogin(
                  typePass: true,
                  typeInput: false,
                  isiField: _passWordBaru,
                  hintText: "Password Baru",
                  prefixIcon: Icons.vpn_key_rounded,
                  isObsercure: statusPass1,
                  suffixIcon: Icons.remove_red_eye,
                  clickCallback: () => _clickCallBackNew()),
            ),
            Container(
              color: colorGrey,
              child: TextFieldLogin(
                  typePass: true,
                  typeInput: false,
                  isiField: _passWordBaruValid,
                  hintText: "Password Baru Validasi",
                  prefixIcon: Icons.vpn_key_rounded,
                  isObsercure: statusPass2,
                  suffixIcon: Icons.remove_red_eye,
                  clickCallback: () => _clickCallBackNewValid(),
                  onChange: ()=> _onChange()),
            ),
            SizedBox(height: 20,),
            BlocListener<BlocLoginBloc, BlocLoginState>(
              bloc: blocPass,
              listener: (context, state) {
                if(state is BlocStateEditPassSukses){
                  tampilSnack();
                }
                if(state is BlocaStateEditPassFailed){
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Ganti Passowrd Salah, "+state.errorMessage.toString()),
                      duration: Duration(milliseconds: 500),
                      backgroundColor: Colors.red,
                    )
                  );
                }
                // TODO: implement listener
              },
              child: Container(
                child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
                  bloc: blocPass,
                  builder: (context, state) {
                    if(state is BlocStateEditPassSukses){
                      return bottomPass(size);
                    }
                    if(state is BlocStateEditPassLoading){
                      return Container(child:Center(child:CircularProgressIndicator()));
                    }
                    if(state is BlocaStateEditPassFailed){
                      return bottomPass(size);
                    }
                    return bottomPass(size);
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding bottomPass(Size size) {
    return Padding(
            padding: EdgeInsets.only(left: 25, right: 25),
            child: (statusButton == true)?TextButton(
              onPressed: (){
                MyDataFormLogin data = MyDataFormLogin(password: _passWord.text.toString(),newPassword: _passWordBaruValid.text.toString());
                blocPass..add(BlocGantiPass(data: data));
              },
              child: Container(
              width: size.width,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(20)
              ),
              child: Padding(
                padding: const EdgeInsets.only(left:20.0),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Icon(Icons.change_circle, size: 35,color: Colors.white,),
                    Text("Ganti Password", style: TextStyle(color: Colors.white),)
                  ],
                ),
              ),
            )):Padding(
                padding: EdgeInsets.only(left: 25, right: 25),
                child: Container(
                  child:Text("Password Baru Tidak Valid", style: TextStyle(color: Colors.red),)
                ),
              ),
          );
  }
}