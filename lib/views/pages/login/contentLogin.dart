import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/login/model_form_login.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/Register/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/admin/home/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/home/main.dart';
// import 'package:flutter_jamu_tujuh/views/admin/pages/menu.dart';
// import 'package:flutter_jamu_tujuh/views/pages/menu.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_button.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_sphare.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_textField.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_jamu_tujuh/views/widgets/register/register.dart';

class BackgroundLogin extends StatefulWidget {
  @override
  _BackgroundLoginState createState() => _BackgroundLoginState();
}

class _BackgroundLoginState extends State<BackgroundLogin> {
  bool statusPass = true;
  BlocLoginBloc bloc = BlocLoginBloc();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          decoration: BoxDecoration(
            // color: background1.withOpacity(0.7),
              // image: DecorationImage(
              //   image: AssetImage("assets/login/login.jpg"),
              //   fit: BoxFit.cover,
              // ),
            ),
          child: Column(
          children: [
            Container(
              color: background1.withOpacity(0.3),
              height: size.height-(size.height/1.4),
              child: Stack(
                children: [
                  Container(
                    color: circlePurpleDark.withOpacity(0.7),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: background1.withOpacity(0.7),
                        // borderRadius:
                        //     BorderRadius.only(bottomLeft: Radius.circular(88)
                        //     )
                            ),
                  )
                ],
              ),
            ),
            Container(
              height: size.height/1.4,
              color: circlePurpleDark.withOpacity(0.7),
             
              child: Stack(
                children: [
                  Container(
                    color: background1.withOpacity(0.7),
                  ),
                  Container(
                    // padding:const EdgeInsets.symmetric(horizontal: 24, vertical: 28),
                    decoration: BoxDecoration(
                        color: circlePurpleDark.withOpacity(0.7),
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(88),topLeft: Radius.circular(88))),
                            child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment:MainAxisAlignment.center,
                            children: [
                              SizedBox(height:15),
                              // Image.asset("assets/logo/logo.png", height: 200,),
                              Text(
                                "Login",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                              ),
                              SizedBox(height:10),
                              // Spacer(),
                              TextFieldLogin(
                                typePass: false,
                                typeInput: false,
                                isiField: _email,
                                hintText: "Email",
                                prefixIcon: Icons.email,
                              ),
                              TextFieldLogin(
                                typePass: true,
                                typeInput: false,
                                  isiField: _passWord,
                                  hintText: "Password",
                                  prefixIcon: Icons.vpn_key_rounded,
                                  isObsercure: statusPass,
                                  suffixIcon: Icons.remove_red_eye,
                                  clickCallback: () => _clickCallBack()),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                width: size.width,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    BlocListener<BlocLoginBloc, BlocLoginState>(
                                    bloc: bloc,
                                    listener: (context, state) {
                                      if(state is BlocStateSukses){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Login Berhasil"),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.green,
                                            onVisible: (){
                                              Navigator.pop(context);
                                              print(state.myData.role);
                                              if(state.myData.role == 1){
                                                Navigator.push(
                                                  context, MaterialPageRoute(builder: (context) => HomeAdmin()));
                                              } else {
                                                Navigator.push(
                                                  context, MaterialPageRoute(builder: (context) => HomeUser()));
                                              }
                                            },
                                          )
                                        );
                                      }
                                      if(state is BlocaStateFailed){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                          SnackBar(
                                            content: Text("Login Gagal, "+state.errorMessage.toString()),
                                            duration: Duration(milliseconds: 500),
                                            backgroundColor: Colors.red,
                                          )
                                        );
                                      }
                                    },
                                    child: Container(
                                      child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
                                        bloc: bloc,
                                        builder: (context, state) {
                                          print(state);
                                          if (state is BlocStateSukses) {
                                            return buildButtonLogin();
                                          }
                                          if (state is BlocStateLoading) {
                                            return Center(
                                              child: CircularProgressIndicator(color: background1,),
                                            );
                                          }
                                          if (state is BlocaStateFailed) {
                                            return buildButtonLogin();
                                          }
                                          if (state is BlocLoginInitial) {
                                            return buildButtonLogin();
                                          }
                                          return Container();
                                        },
                                      ),
                                    ),
                                  ),
                                  ButtonLogin(
                                    onPress: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>BackgroundRegister()));
                                    },
                                    buttonName: "Daftar",
                                    paddingH: 35.0,
                                  )
                                  ],
                                ),
                              ),
                              
                              Spacer()
                            ],
                          ),
                      ),
                  )
                ],
              ),
            )
          ],
            ),
        ),
      ),
    );
  }

  ButtonLogin buildButtonLogin() {
    return ButtonLogin(
      onPress: (){
        print("sign");
        MyDataFormLogin isi = MyDataFormLogin(email: _email.text.toString(), password: _passWord.text.toString());
        bloc..add(BlocEventLogin(myData: isi));
      },
      buttonName: "Masuk",
      paddingH: 35.0,
    );
  }
}
