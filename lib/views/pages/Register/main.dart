import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/login/model_form_login.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_login/bloc_login_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/admin/home/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/home/main.dart';
// import 'package:flutter_jamu_tujuh/views/admin/pages/menu.dart';
// import 'package:flutter_jamu_tujuh/views/pages/menu.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_button.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_sphare.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/login/custom_textField.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_jamu_tujuh/views/widgets/register/register.dart';

class BackgroundRegister extends StatefulWidget {
  @override
  _BackgroundRegisterState createState() => _BackgroundRegisterState();
}

class _BackgroundRegisterState extends State<BackgroundRegister> {
  bool statusPass = true;
  bool statusPage = false;
  BlocLoginBloc bloc = BlocLoginBloc();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _noHp = TextEditingController();
  final TextEditingController _alamat = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  void _clickCallBack() {
    setState(() {
      statusPass = !statusPass;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          decoration: BoxDecoration(
            // color: background1.withOpacity(0.7),
              image: DecorationImage(
                image: AssetImage("assets/login/login.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          child: Column(
          children: [
            Container(
              color: background1.withOpacity(0.3),
              height: size.height-(size.height/1.4),
              child: Stack(
                children: [
                  Container(
                    color: circlePurpleDark.withOpacity(0.7),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: background1.withOpacity(0.7),
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(88))),
                  )
                ],
              ),
            ),
            Container(
              height: size.height/1.4,
              color: circlePurpleDark.withOpacity(0.7),
             
              child: Stack(
                children: [
                  Container(
                    color: background1.withOpacity(0.7),
                  ),
                  Container(
                    // padding:const EdgeInsets.symmetric(horizontal: 24, vertical: 28),
                    decoration: BoxDecoration(
                        color: circlePurpleDark.withOpacity(0.7),
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(88))),
                            child: Container(
                  alignment: Alignment.center,
                  child: Container(
                    width: size.width * 0.8,
                    height: size.height * 0.65,
                    child: Column(
                      children: [
                        Spacer(),
                        Text(
                          "Registrasi",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30.0,
                              fontWeight: FontWeight.bold,fontFamily: 'RobotoCondensed'),
                        ),
                        Spacer(),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: false,
                          isiField: _username,
                          hintText: "Username",
                          prefixIcon: Icons.account_circle_rounded,
                        ),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: false,
                          isiField: _email,
                          hintText: "Email",
                          prefixIcon: Icons.email,
                        ),
                        TextFieldLogin(
                          typePass: true,
                          typeInput: false,
                            isiField: _passWord,
                            hintText: "Password",
                            prefixIcon: Icons.vpn_key_rounded,
                            isObsercure: statusPass,
                            suffixIcon: Icons.remove_red_eye,
                            clickCallback: () => _clickCallBack()),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: true,
                          isiField: _noHp,
                          hintText: "Nomor Hp",
                          prefixIcon: Icons.phone_android,
                        ),
                        TextFieldLogin(
                          typePass: false,
                          typeInput: false,
                          isiField: _alamat,
                          hintText: "Alamat",
                          prefixIcon: Icons.home,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          width: size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              BlocListener<BlocLoginBloc, BlocLoginState>(
                              bloc: bloc,
                              listener: (context, state) {
                                if(state is BlocStateRegistSukses){
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text("Registrasi Berhasil"),
                                      duration: Duration(milliseconds: 500),
                                      backgroundColor: Colors.green,
                                      onVisible: (){
                                        
                                      },
                                    )
                                  );
                                  Navigator.pop(context);
                                }
                              },
                              child: Container(
                                child: BlocBuilder<BlocLoginBloc, BlocLoginState>(
                                  bloc: bloc,
                                  builder: (context, state) {
                                    if (state is BlocStateRegistSukses) {
                                      return buildButtonLogin();
                                    }
                                    if (state is BlocStateRegistLoading) {
                                      return Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    if (state is BlocaStateFailed) {
                                      return buildButtonLogin();
                                    }
                                    if (state is BlocLoginInitial) {
                                      return buildButtonLogin();
                                    }
                                    return Container();
                                  },
                                ),
                              ),
                            ),
                            ButtonLogin(
                              onPress: (){
                                Navigator.pop(context);
                              },
                              buttonName: "Kembali",
                              paddingH: 35.0,
                            )
                            ],
                          ),
                        ),
                        Spacer()
                      ],
                    ),
                  ),
                ),
                  )
                ],
              ),
            )
          ],
            ),
        ),
      ),
    );
  }

  ButtonLogin buildButtonLogin() {
    return ButtonLogin(
      onPress: (){
        print("sign");
        MyDataFormLogin isi = MyDataFormLogin(email: _email.text.toString(), password: _passWord.text.toString(), username: _username.text.toString(), noHp: _noHp.text.toString(), alamat: _alamat.text.toString());
        bloc..add(BlocRegister(data: isi));
      },
      buttonName: "Daftar",
      paddingH: 35.0,
    );
  }
}
