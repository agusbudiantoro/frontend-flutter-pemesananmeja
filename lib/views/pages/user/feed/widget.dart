import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_barang/barang_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_kategori/kategori_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/detailBarang/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/keranjang/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmerContentFeed.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmermiddle2Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/middle2Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/data/content.dart';
import 'dart:math';
// import 'package:flutter_jamu_tujuh/views/widgets/keranjang/backgroundKeranjang.dart';

class IsiFeedBarang extends StatefulWidget {
  final int jenis;

  IsiFeedBarang({this.jenis});
  @override
  _IsiFeedBarangState createState() => _IsiFeedBarangState(jenis: this.jenis);
}

class _IsiFeedBarangState extends State<IsiFeedBarang> with SingleTickerProviderStateMixin{
  List<ValuesKategori> listKat;
  List<BarangModel> listIsi;
  final int jenis;
  BarangBloc blocBarang = BarangBloc();
  KategoriBloc blocKategori = KategoriBloc();
  ValuesKategori valueKat=ValuesKategori(id: 0, namaKategori: "Semua");
  String cariBarang="";
  int toggle = 0;
  AnimationController _con;
  TextEditingController _textEditingController;
  _IsiFeedBarangState({this.jenis});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingController = TextEditingController();
    _con = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 375),
    );
    print(jenis);
    blocKategori..add(GetKategoriEvent());
    if (jenis == 1) {
      blocBarang..add(EventGetBarangRekom());
    } else if (jenis == 2) {
      blocBarang..add(EventGetBarangPromo());
    } else if (jenis == 3) {
      print("jenis 3");
        setState(() {
          if (toggle == 0) {
              toggle = 1;
              _con.forward();
            }
        });
      // listIsi = dataBarangRek() + dataBarangPro();
      blocBarang..add(EventGetBarang());
    }
  }

  doSomethingKetgori(ValuesKategori data) { 
    setState(() {
      valueKat=(data != null)?data:ValuesKategori(id: 0, namaKategori: "Semua");
    });
    print(valueKat.namaKategori);
    print(valueKat.id);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          color: background1,
          padding: EdgeInsets.only(top:size.height/15, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
              child: IconButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back_ios, color: colorGrey,),
              ),
            ),
              Container(
          height:size.height/18,
          width: size.width/1.5,
          alignment: Alignment(1.0, 0.0),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 375),
            height: 48.0,
            width: (toggle == 0) ? 48.0 : 250.0,
            curve: Curves.easeOut,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  spreadRadius: -1.0,
                  blurRadius: 1.0,
                  offset: Offset(0.0, 1.0),
                ),
              ],
            ),
            child: Stack(
              children: [
                AnimatedPositioned(
                  duration: Duration(milliseconds: 375),
                  left: (toggle == 0) ? 20.0 : 40.0,
                  curve: Curves.easeOut,
                  top: 11.0,
                  child: AnimatedOpacity(
                    opacity: (toggle == 0) ? 0.0 : 1.0,
                    duration: Duration(milliseconds: 200),
                    child: Container(
                      height: 23.0,
                      width: 180.0,
                      child: TextField(
                        onChanged: (val){
                          setState(() {
                            cariBarang=val.toString();
                          });
                        },
                        autofocus: (widget.jenis == 3)?true:false,
                        cursorRadius: Radius.circular(10.0),
                        cursorWidth: 2.0,
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          labelText: 'Pencarian',
                          labelStyle: TextStyle(
                            color: Color(0xff5B5B5B),
                            fontSize: 17.0,
                            fontWeight: FontWeight.w500,
                          ),
                          alignLabelWithHint: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Material(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30.0),
                  child: IconButton(
                    splashRadius: 19.0,
                    icon: Icon(Icons.search_sharp),
                    onPressed: () {
                      print(_con);
                      print("cek forward");
                      print(_con.forward());
                      setState(
                        () {
                          if (toggle == 0) {
                            toggle = 1;
                            _con.forward();
                          } else {
                            toggle = 0;
                            _textEditingController.clear();
                            _con.reverse();
                          }
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
            Container(
              child: IconButton(
                color: Colors.white,
                onPressed: () {
                  Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BackgroundKeranjang()));
                },
                icon: Icon(Icons.shopping_cart_outlined,color: colorGrey,),
              ),
            )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 5),
          width: size.width,
          height: size.height / 15,
          child: BlocBuilder<KategoriBloc, KategoriState>(
                          bloc:blocKategori,
                          builder: (context, state) {
                            if(state is KategoriStateSukses){
                              return Middle2Container(listKat: state.myData,callback: doSomethingKetgori);
                            }
                            if(state is KategoriStateLoading){
                              return ShimmerMiddle2Container();
                            }
                            if(state is KategoriStateFailed){
                              return Container(child: Center(child: Text("Gagal Ambil Data Kategori"),),);
                            }
                            return Container();
                          },
                        ),
        ),
        Container(
            child: BlocBuilder<BarangBloc, BarangState>(
            bloc: blocBarang,
            builder: (context, state) {
              
              if(state is StateGetBarangSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangWaiting){
                return ShimmerContentFeed();
              }
              if(state is StateGetBarangFailed){
                return Container(child: Center(child: Text("Gagal Mengambil Data"),),);
              }
              if(state is StateDelBarangFailed){
                return Container(child: Center(child: Text("Gagal Hapus Data"),),);
              }
              if(state is StateDelBarangWaiting){
                return ShimmerContentFeed();
              }
              if(state is StateGetBarangRekomSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangRekomWaiting){
                return ShimmerContentFeed();
              }
              if(state is StateGetBarangRekomFailed){
                return Container(child: Text("Gagal Mengambil Data"),);
              }
              if(state is StateGetBarangPromoSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangPromoWaiting){
                return ShimmerContentFeed();
              }
              if(state is StateGetBarangPromoFailed){
                return Container(child: Text("Gagal Mengambil Data"),);
              }
              return Container();
            },
          ),
          ),
        
      ],
    );
  }

  Expanded listBodyFeed(Size size,List<ValuesListBarang> data) {
      List<ValuesListBarang> listData = List<ValuesListBarang>();
      if(cariBarang.toString().length >0){
      if(valueKat.id == 0){
      listData = data.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase())).toList();
      } else if(valueKat.id != 0){
      listData = data.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase()) && i.kategori == valueKat.id).toList();
      }
      } else if(cariBarang.toString().length == 0){
      if(valueKat.id == 0){
      listData = data;
      } else if(valueKat.id != 0){
      listData = data.where((i) => i.kategori == valueKat.id).toList();
      }
      }
    return Expanded(
          child: Padding(
        padding: EdgeInsets.all(5),
        child: GridView.builder(
          scrollDirection: Axis.vertical,
          itemCount: listData.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailProduk(
                              idBarang: listData[index].idBarang,
                              images: listData[index].gambar.toString(),
                              hargaPromo: listData[index].hargaPromo.toString(),
                              namaBarang: listData[index].namaBarang.toString(),
                              harga: listData[index].harga.toString(),
                              deskripsi: listData[index].deskripsi.toString(),
                              qty: listData[index].qty,
                              diskon: listData[index].promo.toString(),
                            )));
              },
              child: Container(
                // height: 10,
                child: Column(
                  children: [
                    Expanded(
                      flex:2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: Container(
                          padding: EdgeInsets.all(10),
                                color:contentColor,
                                width: size.width,
                                height: size.height / 6,
                                child: new Image.network(
                                  domain+'gambar/'+listData[index].gambar.toString(),
                                  fit: BoxFit.contain,
                                )),
                      ),
                    ),
                    // Container(
                    //   height: 100,
                    //   width: 200,
                    //   child: Stack(
                    //     children: [
                    //       Container(
                    //           color: Colors.transparent,
                    //           width: size.width,
                    //           height: size.height / 6,
                    //           child: new Image.network(
                    //             domain+'gambar/'+listData[index].gambar.toString(),
                    //             fit: BoxFit.contain,
                    //           )),
                    //     ],
                    //   ),
                    // ),
                    Container(
                      padding: EdgeInsets.only(top:10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top:5,bottom: 5,left: 10),
                            child: Text(listData[index].namaBarang.toString(),style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold)),
                          ),
                          (listData[index].promo != 0)?Container(
                            padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right:10),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  listData[index].promo.toString()+"%",
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                )):Container(),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          (listData[index].promo != 0)
                        ? Container(
                          alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(top:5,bottom: 5,left: 10),
                            child: Text(
                                "Rp "+listData[index].hargaPromo.toString(),style: TextStyle(color: circlePurpleDark, fontWeight: FontWeight.w600)),
                          )
                        : Container(
                              alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right: 10),
                                child: Text("Rp "+listData[index].harga.toString()),
                              ),
                        (listData[index].promo == 0)
                            ? Container(
                              alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right: 10),
                              )
                            : Container(
                              alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right: 10),
                                child: Text(
                                  "Rp "+listData[index].harga.toString(),
                                  style: TextStyle(
                                    color: Colors.red,
                                      fontSize: 12,
                                      decoration:
                                          TextDecoration.lineThrough),
                                ),
                              ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 5,
            mainAxisSpacing: 5,
          ),
        ),
      ));
  }
}
