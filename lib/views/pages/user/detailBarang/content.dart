import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/pagePembayaran.dart/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/detailProduk/custom_container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/modalBuy/modalBuy.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/modalBuy/modalBuy2.dart';

class BackgroundDetailProduk extends StatefulWidget {
  final String images;
  final String harga;
  final String hargaPromo;
  final String diskon;
  final String namaBarang;
  final String deskripsi;
  final int qty;
  final int idBarang;

  BackgroundDetailProduk(
      {this.diskon,
      this.harga,
      this.hargaPromo,
      this.images,
      this.namaBarang,
      this.deskripsi,
      this.qty,
      this.idBarang});
  @override
  _BackgroundDetailProdukState createState() => _BackgroundDetailProdukState();
}

class _BackgroundDetailProdukState extends State<BackgroundDetailProduk> {
  bool statusPass = false;
  bool openDesk = false;
  int stokBarang;
  // BlocLoginBloc bloc = BlocLoginBloc();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stokBarang = widget.qty;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Container(
              height: size.height / 1.4,
              child: Stack(
                children: [
                  Container(
                    color: Colors.transparent,
                  ),
                  Container(
                    height: size.height / 1.3,
                    decoration: BoxDecoration(
                        color: background1,
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(88))),
                    child: Row(
                      children: [
                        Container(
                            padding: EdgeInsets.only(top: 40),
                            height: size.height / 1.4,
                            alignment: Alignment.topCenter,
                            width: size.width / 7,
                            child: Column(
                              children: [
                                TextButton(
                                    style: ButtonStyle(
                                      overlayColor: MaterialStateProperty.all(colorGrey)
                                    ),
                                    onPressed: (){
                                      Navigator.pop(context);
                                    }, 
                                    child: Card(
                                      elevation: 10,
                                      child: Container(
                                        // padding: EdgeInsets.all(8),
                                        alignment: Alignment.center,
                                        height: size.height/20,
                                        width: size.width/4,
                                        decoration: BoxDecoration(
                                          color: contentColor,
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Icon(Icons.arrow_back, color: Colors.black),
                                          ],
                                        ),
                                      ),
                                    )),
                                    SizedBox(height: 10,),
                                    TextButton(
                                    style: ButtonStyle(
                                      overlayColor: MaterialStateProperty.all(colorGrey)
                                    ),
                                    onPressed: (){
                                      BarangModel myData = BarangModel(stok: widget.qty,image: widget.images,totalHarga: 0, idBarang: widget.idBarang, deskripsi: widget.deskripsi, harga: widget.harga.toString(), hargaPromo: widget.hargaPromo.toString(),namaBarang: widget.namaBarang, qty: 1, diskon: widget.diskon.toString());
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>Pagebayar(dataBarang: myData,)));
                                    }, 
                                    child: Card(
                                      elevation: 10,
                                      child: Container(
                                        height: size.height/20,
                                        width: size.width/4,
                                        // padding: EdgeInsets.all(8),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          color: contentColor,
                                          borderRadius: BorderRadius.circular(5)
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: [
                                            Icon(Icons.payments_sharp, color: Colors.green),
                                          ],
                                        ),
                                      ),
                                    )),
                                    SizedBox(height: 10,),
                                    TextButton(
                                    style: ButtonStyle(
                                      overlayColor: MaterialStateProperty.all(colorGrey)
                                    ),
                                    onPressed: (){
                                      BarangModel myBarang = BarangModel(
                                              idBarang: widget.idBarang,
                                              deskripsi: widget.deskripsi,
                                              harga: widget.harga,
                                              hargaPromo: widget.hargaPromo,
                                              namaBarang: widget.namaBarang,
                                              qty: widget.qty,
                                              image: widget.images,
                                              diskon: widget.diskon,
                                              statusCeklis: false);
                                          showModalBottomSheet(
                                            backgroundColor: colorGrey,
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(88), topRight: Radius.circular(88))),
                                              context: context,
                                              builder: (context) {
                                                return ModalBuy2(
                                                  data: myBarang,
                                                );
                                              });
                                    }, 
                                    child: Card(
                                      elevation: 10,
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: size.height/20,
                                          width: size.width/4,
                                        decoration: BoxDecoration(
                                          color: contentColor,
                                          borderRadius: BorderRadius.circular(5)
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: [
                                            Icon(Icons.shopping_cart, color: colorGrey),
                                          ],
                                        ),
                                      ),
                                    )),
                              ],
                            )),
                        (openDesk == false)
                            ? Container(
                                child: Stack(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      child: new Image.network(
                                        domain+'gambar/' +
                                            widget.images.toString(),
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    (widget.diskon != '0')
                                        ? Positioned(
                                            top: -20,
                                            right: -45,
                                            child: Container(
                                                alignment: Alignment.center,
                                                width: 100,
                                                height: 20,
                                                transform:
                                                    Matrix4.rotationZ(0.8),
                                                color: Colors.red,
                                                child: Text(
                                                  widget.diskon.toString(),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                )),
                                          )
                                        : Container()
                                  ],
                                ),
                                height: size.height / 1.4,
                                width: size.width - (size.width / 7),
                              )
                            : Container(
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Text(widget.deskripsi.toString(),
                                          style: TextStyle(
                                              fontFamily: 'RobotoCondensed'),
                                          textAlign: TextAlign.left),
                                    ],
                                  ),
                                ),
                                height: size.height / 1.4,
                                width: size.width - (size.width / 7),
                              )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: size.height / 3.2,
              child: Stack(
                children: [
                  Container(
                    color: background1,
                  ),
                  Container(
                    
                    decoration: BoxDecoration(
                        color: circlePurpleDark,
                        borderRadius:
                            BorderRadius.only(topRight: Radius.circular(88), topLeft: Radius.circular(88))),
                    child: Container(
                      height: size.height - (size.height / 1.5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 50,
                              right: 10,
                              left: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(widget.namaBarang,
                                    style: TextStyle(
                                        color: captionColor,
                                        fontSize: 25.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'RobotoCondensed')),
                                Text("Rp " + widget.harga,
                                    style: TextStyle(
                                        color: (widget.diskon == "0")
                                            ? captionColor
                                            : Colors.red,
                                        decoration: (widget.diskon == "0")
                                            ? TextDecoration.none
                                            : TextDecoration.lineThrough,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'RobotoCondensed'))
                              ],
                            ),
                          ),
                          (widget.diskon != '0')
                              ? Container(
                                  padding: EdgeInsets.only(
                                    top: 10,
                                    right: 10,
                                    left: 10,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Harga Promo",
                                          style: TextStyle(
                                              color: captionColor,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'RobotoCondensed')),
                                      Text("Rp " + widget.hargaPromo,
                                          style: TextStyle(
                                              color: captionColor,
                                              fontSize: 15.0,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: 'RobotoCondensed'))
                                    ],
                                  ),
                                )
                              : Container(),
                          Container(
                            padding: EdgeInsets.only(
                              top: 10,
                              right: 10,
                              left: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Stok",
                                    style: TextStyle(
                                        color: captionColor,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'RobotoCondensed')),
                                Text(widget.qty.toString(),
                                    style: TextStyle(
                                        color: captionColor,
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'RobotoCondensed'))
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                      width: size.width / 2,
                                      height: 74,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          setState(() {
                                            openDesk = false;
                                          });
                                        },
                                        child: Text(
                                          "Barang",
                                          style: TextStyle(
                                              color: (openDesk == true)
                                                  ? Colors.black
                                                  : Colors.black),
                                        ),
                                        style: ButtonStyle(
                                            elevation: MaterialStateProperty.all<double>(
                                                0),
                                            backgroundColor:
                                                MaterialStateProperty.all<Color>(
                                                    (openDesk == true)
                                                        ? contentColor
                                                        : background1),
                                            shape: MaterialStateProperty.all<
                                                    RoundedRectangleBorder>(
                                                RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.only(
                                                        topRight: Radius.circular(20))))),
                                      ),
                                    ),
                              Container(
                                width: size.width / 2,
                                height: 74,
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      openDesk = true;
                                    });
                                  },
                                  child: Text(
                                    "Deskripsi",
                                    style: TextStyle(
                                        color: (openDesk == true)
                                            ? Colors.black
                                            : Colors.black),
                                  ),
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              (openDesk == true)
                                                  ? background1
                                                  : contentColor),
                                      elevation:
                                          MaterialStateProperty.all<double>(0),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(20))))),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
