import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

class ListMenu extends StatefulWidget {
  IconData iconMenu;
  String judul;
  double fontJudul;
  double sizeHeight;
  double sizeWidth;
  Color backColor;
  ListMenu(
      {this.iconMenu,
      this.judul,
      this.fontJudul,
      this.sizeHeight,
      this.sizeWidth,
      this.backColor
      });
  @override
  _ListMenuState createState() => _ListMenuState();
}

class _ListMenuState extends State<ListMenu> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: widget.sizeHeight / 8,
      width: 70,
      child: Column(
        children: [
          Container(
            height: widget.sizeHeight / 18,
            width: widget.sizeWidth,
            decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10))),
            child: Container(
                margin: EdgeInsets.only(top: 10),
                child: Icon(
                  widget.iconMenu,
                  color: background1,
                  size: 35,
                )),
          ),
          Container(
            height: widget.sizeHeight / 8 - (widget.sizeHeight / 18),
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.judul,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: widget.fontJudul,
                  ),
                  textAlign: TextAlign.center,
                )
              ],
            ),
          )
        ],
      ),
      decoration: BoxDecoration(
          color: widget.backColor, borderRadius: BorderRadius.circular(10)),
    );
  }
}
