import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:flutter_jamu_tujuh/models/jadwalbookmeja/main.dart';
import 'package:flutter_jamu_tujuh/models/meja_tersedia/main.dart';
import 'package:flutter_jamu_tujuh/models/model_paket_waktu/model_paket_waktu.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_meja/bloc_meja_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/paketwaktu_bloc/paketwaktu_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanLapangan_bloc/pesanlapangan_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/pesanMeja/listMeja.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/dialog_delete/dialog_delete.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/textField/textField1.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PagePesanMeja extends StatefulWidget {
  const PagePesanMeja();

  @override
  _PagePesanMejaState createState() => _PagePesanMejaState();
}

class _PagePesanMejaState extends State<PagePesanMeja> {
  TextEditingController tgl_book =  TextEditingController();
  TextEditingController paket_waktu =  TextEditingController();
  PesanlapanganBloc blocPesanLapangan = PesanlapanganBloc();
  PesanlapanganBloc DelblocPesanLapangan = PesanlapanganBloc();
  PaketwaktuBloc blocPaketWaktu = PaketwaktuBloc();
  BlocMejaBloc blocMejaBloc = BlocMejaBloc();
  String myValue='10.00 - 11.00';
  DateTime datenow = new DateTime.now();
  int idKategori=1;
  String pilihMeja;
  int pilihIdMeja;
  int idTim;
  String nohp;
  String namatim;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    blocPaketWaktu..add(EventGetPaketWaktu());
    
    blocMejaBloc..add(EventGetKetersediaanMeja(paket_waktu: idKategori, tgl: datenow));
    blocPesanLapangan..add(EventGetPesananById());
    getData();
  }

  void getData()async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idTim = preferences.getInt("myId");
      nohp = preferences.getString("noHp");
      namatim = preferences.getString("username");
    });
    print(idTim);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: BlocListener<PesanlapanganBloc, PesanlapanganState>(
        bloc: blocPesanLapangan,
        listener: (context, state) {
          if(state is StatePostPesanLapanganSukses){
            blocPesanLapangan..add(EventGetPesananById());
            blocMejaBloc..add(EventGetKetersediaanMeja(paket_waktu: idKategori, tgl: datenow));
          }
          // TODO: implement listener
        },
        child: Container(
          child: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
        bloc: blocPesanLapangan,
        builder: (context, state) {
          if(state is StateDelPesanLapanganSukses){
            return (pilihIdMeja != null)?FloatingActionButton(
        onPressed: (){
          ModelListJadwal formPesan = ModelListJadwal(idTim: idTim, noTelp: nohp,tanggalPemesanan: tgl_book.text,namaTim: namatim, nomorMeja: pilihMeja.toString(),paketWaktu: idKategori.toString(), idMeja: pilihIdMeja); 
          blocPesanLapangan..add(EventPostPEsanLapangan(data: formPesan));
        },
        child: Icon(Icons.send),
      ):Container();
          }
          if(state is StateDelPesanLapanganFailed){
            return (pilihIdMeja != null)?FloatingActionButton(
        onPressed: (){
          ModelListJadwal formPesan = ModelListJadwal(idTim: idTim, noTelp: nohp,tanggalPemesanan: tgl_book.text,namaTim: namatim, nomorMeja: pilihMeja.toString(),paketWaktu: idKategori.toString(),idMeja: pilihIdMeja); 
          blocPesanLapangan..add(EventPostPEsanLapangan(data: formPesan));
        },
        child: Icon(Icons.send),
      ):Container();
          }
          if(state is StateDelPesanLapanganWaiting){
            return Container(child: Center(child: CircularProgressIndicator(),),);
          }
          return (pilihIdMeja != null)?FloatingActionButton(
        onPressed: (){
          ModelListJadwal formPesan = ModelListJadwal(idTim: idTim, noTelp: nohp,tanggalPemesanan: tgl_book.text,namaTim: namatim, nomorMeja: pilihMeja.toString(),paketWaktu: idKategori.toString(),idMeja: pilihIdMeja); 
          blocPesanLapangan..add(EventPostPEsanLapangan(data: formPesan));
        },
        child: Icon(Icons.send),
      ):Container();
        },
      ),
        ),
      ),
      body: ListView(
        physics: AlwaysScrollableScrollPhysics(),
        children: [
          Container(
            child: Column(
              children:[
                TextFieldRegister(
                                typePass: false,
                                typeInput: false,
                                readOnly: true,
                                onTap: () => _showFilter(context),
                                isiField: tgl_book,
                                hintText: "Tanggal Pesan Meja",
                                prefixIcon: Icons.date_range,
                              ),
                              BlocBuilder<PaketwaktuBloc, PaketwaktuState>(
                      bloc:blocPaketWaktu,
                      builder: (context, state) {
                        if(state is StateGetPaketWaktuSukses){
                          return dripDownButton(state.data);
                          // return TexFieldCustom(isi: waktuPemesanan, containerField: "Waktu Pemesanan",tapCallBackWaktu:()=> myCallBackWaktu(state.data),readOnly: false,);
                        }
                        if(state is StateGetPaketWaktuWaiting){
                          return Container(child:Center(child:CircularProgressIndicator()));
                        }
                        if(state is StateGetPaketWaktuFailed){
                          return Container(child:Center(child:Text("gagal ambil data")));
                        }
                        return Container();
                      },
                    ),
              ]
            ),
          ),
          Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Container(child: Text("Pilih Meja: "+pilihMeja.toString(), style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),),
                  Container(
                    alignment: Alignment.topCenter,
                    margin: EdgeInsets.only(top:10,right: 20, left: 20),
                    child: BlocBuilder<BlocMejaBloc, BlocMejaState>(
                      bloc: blocMejaBloc,
                      builder: (context, state) {
                        if(state is StateGetKetersediaanMejaSukses){
                          return ListMeja(size, state.data);
                        }
                        if(state is StateGetKetersediaanMejaLoad){
                          return Container(child: Center(child: CircularProgressIndicator(),),);
                        }
                        if(state is StateGetKetersediaanMejaFailed){
                          return Container(child: Center(child: Text("Failed"),),);
                        }
                        return Container();
                      },
                    ),
                    
                    ),
                    Container(
                  child: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
                    bloc: blocPesanLapangan,
                    builder: (context, state) {
                      print(state);
                      if(state is StateGetPesanLapanganByIdTimSukses){
                        return ListViewChild(size, state.data);
                      }
                      if(state is StateGetPesanLapanganByIdTimWaiting){
                        return Container(child: Center(child: CircularProgressIndicator(),),);
                      }
                      if(state is StateGetPesanLapanganByIdTimFailed){
                        return Container(child: Center(child: Text("Gagal Memuat Data"),),);
                      }
                      return Container();
                    },
                  ),
                )
                ],
              )
                ),
                
        ],
      ),
    );
  }

  ListView ListViewChild(Size size, List<ModelListJadwal> data) {
    return ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: data.length,
              itemBuilder: (BuildContext context, int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/5,
                  width: size.width,
                  decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 2),
                          color: colorGrey, 
                          borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Container(
                                  margin: EdgeInsets.all(15),
                                  // color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Text(data[i].tanggalPemesanan, style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.normal)))),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Waktu : "+data[i].jam.toString(), style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal))),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Meja : "+data[i].nomorMeja.toString(), style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal))),
                                        )
                                    ],
                                  ),
                                )),
                                Expanded(
                                flex: 1,
                                child: BlocListener<PesanlapanganBloc, PesanlapanganState>(
                                  bloc: DelblocPesanLapangan,
                                  listener: (context, state) {
                                    if(state is StateDelPesanLapanganSukses){
                                      blocPesanLapangan..add(EventGetPesananById());
                                      blocMejaBloc..add(EventGetKetersediaanMeja(paket_waktu: idKategori, tgl: datenow));
                                    }
                                    // TODO: implement listener
                                  },
                                  child: Container(
                                    child: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
                                      bloc: DelblocPesanLapangan,
                                      builder: (context, state) {
                                        if(state is StateDelPesanLapanganSukses){
                                          return deleteGesture(data, i, size);
                                        }
                                        if(state is StateDelPesanLapanganFailed){
                                          return  deleteGesture(data, i, size);
                                        }
                                        if(state is StateDelPesanLapanganWaiting){
                                          return Container(child: Center(child: CircularProgressIndicator(),),);
                                        }
                                        return deleteGesture(data, i, size);
                                      },
                                    ),
                                  ),
                                )
                                )  
                            ],
                          ),
                );
              }
              );
  }

  GestureDetector deleteGesture(List<ModelListJadwal> data, int i, Size size) {
    return GestureDetector(
                                onTap: (){
                                  _clickCallBackDelete(data[i].id);
                                  // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>ListAgendaAdmin(id: data[i].idBidang,)));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                  // border: Border.all(color: blackMetalic, width: 2),
                                    color: Colors.red, 
                                    borderRadius: BorderRadius.only(topRight: Radius.circular(20))),
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Icon(Icons.delete, size: size.width/10,color: Colors.white,),
                                      )
                                    ],
                                  )
                                ),
                              );
  }

  Future<void> _clickCallBackDelete(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogDelete(id:id,callback: callbackDelete,)
            );
  }

  callbackDelete(int id){
    print("delete");
    print(id);
    DelblocPesanLapangan..add(EventDeletePesanan(id: id));
  }
  

  Wrap ListMeja(Size size, List<ModelListMejaTersedia> isi) {
    return Wrap(
                  children: isi.map((e) => (e.status == false)?TextButton(
                    onPressed: (){
                      print(e.status);
                      setState(() {
                        pilihIdMeja = e.idMeja;
                        pilihMeja = e.nomorMeja;
                      });
                      print(pilihIdMeja);
                    },
                    child: ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.chair,judul: "Meja "+e.nomorMeja.toString(),fontJudul: 14, backColor: Colors.black,)):
                    TextButton(
                    onPressed: (){
                      print(e.status);
                    },
                    child: ListMenu(sizeHeight: size.height, sizeWidth: size.width,iconMenu: Icons.chair,judul: "Meja "+e.nomorMeja.toString(),fontJudul: 14, backColor: Colors.grey, ))).toList()
                );
  }

  DropdownButton<String> dripDownButton(List<ValuePaketWaktu> dataKategori) {
    
    List<String> isi = [];
    dataKategori.forEach((element) {
      isi.add(element.jam.toString());
    });
    return DropdownButton(
      dropdownColor: Colors.white,
      value: myValue,
      icon: Icon(Icons.keyboard_arrow_down),
      items: isi.map((String items) {
        return DropdownMenuItem(value: items, child: Container(color: Colors.transparent ,child: Text(items.toString(), style: TextStyle(color: Colors.black),)));
      }).toList(),
      onChanged: (String newValue) {
        setState(() {
          myValue = newValue;
          dataKategori.forEach((element) {
            if (element.jam == myValue) {
              idKategori = int.parse(element.paketJam);
              blocMejaBloc..add(EventGetKetersediaanMeja(paket_waktu: idKategori, tgl: datenow));
              // cekLapangan(ModelFormCekKetersediaanLapangan(paketWaktu: idKategori.toString(),lapangan: myValueLap.toString(),tanggalPemesanan: tanggalPemesanan.text.toString()));
            }
          });
        });
      },
    );
  }

  _showFilter(BuildContext context) async {
    print("download"); 
    var datePicked = await DatePicker.showSimpleDatePicker(
                context,
                initialDate: DateTime(2022),
                firstDate: DateTime(1960),
                lastDate: DateTime(2023),
                dateFormat: "yyyy-MMMM-dd",
                locale: DateTimePickerLocale.id,
                looping: true,
              );
              blocMejaBloc..add(EventGetKetersediaanMeja(paket_waktu: idKategori, tgl: datePicked));
              final snackBar =
                  SnackBar(content: Text("Date Picked $datePicked"));
                  setState(() {
                    print(datePicked);
                    if(datePicked != null){
                      datenow = datePicked;
                      tgl_book.text = datePicked.toString().substring(0,10);
                    }
                  });
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}