import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/metodePembayaran/metodePembayaran.dart';
import 'package:flutter_jamu_tujuh/models/pesanan/modelRiwayatPesanan.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanan_bloc/pesanan_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/historyPembelian/historyPembelian.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/alamatPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/daftarBelanja.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/daftarBelanjaPageBayar.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/metodePembayaran.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/metodePengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/rincianHarga.dart';

class Pagebayar extends StatefulWidget {
  final BarangModel dataBarang;
  Pagebayar({this.dataBarang, });
  @override
  _PagebayarState createState() => _PagebayarState();
}

class _PagebayarState extends State<Pagebayar> {
  ValuesMetodePembayaranModel dataPembayaran = ValuesMetodePembayaranModel();
  PesananBloc blocPesanan = PesananBloc();
  int qty;
  int total_hargaBarang;
  int idMetodePembayaran;
  String namaMetodePembayaran="Belum Dipilih";
  String alamatPengiriman;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.dataBarang.qty);
    print(widget.dataBarang.stok);
    qty = widget.dataBarang.qty;
    total_hargaBarang = widget.dataBarang.totalHarga;
  }
  doSomethingMetodePembayaran(ValuesMetodePembayaranModel data) { 
    setState(() {
      idMetodePembayaran=(data != null)?data.idMetodePembayaran:0;
      namaMetodePembayaran=(data != null)?data.namaPembayaran:"Belum Dipilih";
    });
  }
  doSomethingAlamatPengiriman(String alamat) { 
    setState(() {
      alamatPengiriman=alamat;
    });
    print("pagebayar");
    print(alamatPengiriman);
  }

  void _clickCallBackPlus() {
    setState(() {
      if (qty < widget.dataBarang.stok && qty >= 1) {
        qty += 1;
        if(int.parse(widget.dataBarang.diskon) != 0){
          total_hargaBarang = int.parse(widget.dataBarang.hargaPromo) * qty;
        } else {
          total_hargaBarang = int.parse(widget.dataBarang.harga) * qty;
        }
      }
    });
  }

  void _clickCallBackMin() {
    setState(() {
      if (qty <= widget.dataBarang.stok && qty > 1) {
        qty -= 1;
        if(int.parse(widget.dataBarang.diskon) != 0){
          total_hargaBarang = int.parse(widget.dataBarang.hargaPromo) * qty;
        } else {
          total_hargaBarang = int.parse(widget.dataBarang.harga) * qty;
        }
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: contentColor,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: colorGrey,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Pemesanan",
                    style: TextStyle(fontSize: 14, color: colorGrey),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.grey[350],
        child: ListView(
          children: [
            SizedBox(
              height: 10,
            ),
            WidgetAlamat(width: size.width,height: size.height,callback: doSomethingAlamatPengiriman),
            SizedBox(
              height: 10,
            ),
            WidgetMetodePembayaran(height: size.height,width: size.width,callback: doSomethingMetodePembayaran,),
            SizedBox(
              height: 5,
            ),
            WidgetDaftarBelanjaPageBayar(height: size.height,width: size.width,dataBarang: widget.dataBarang,clickCallbackPlus: ()=>_clickCallBackPlus(),clickCallbackMin: ()=>_clickCallBackMin()),
            WidgetPengiriman(height: size.height,width: size.width),
            SizedBox(
              height: 10,
            ),
            WidgetRincianHarga(height: size.height,width: size.width, totalHargaBarang: total_hargaBarang,)
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(10),
        alignment: Alignment.centerLeft,
        height: size.height/9,
        color: Colors.white,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Pembayaran", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                Text("Rp "+(total_hargaBarang+10000).toString(), style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold))
              ],
            ),
            BlocListener<PesananBloc, PesananState>(
              bloc: blocPesanan,
              listener: (context, state) {
                // TODO: implement listener
                if (state is StatePostPesananSukses) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Transaksi Berhasil"),
                      duration: Duration(milliseconds: 500),
                      backgroundColor: Colors.green,
                      onVisible: (){
                        Navigator.pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>BackgroundHistori()));
                      },
                    )
                  );
                }
                if (state is StatePostPesananFailed) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Transaksi Gagal, "+state.errorMessage.toString()),
                      duration: Duration(milliseconds: 500),
                      backgroundColor: Colors.red,
                    )
                  );
                }
              },
              child: Container(
                child: BlocBuilder<PesananBloc, PesananState>(
                bloc: blocPesanan,
                builder: (context, state) {
                  if(state is StatePostPesananSukses){
                    return buttonBayar(size);
                  }
                  if(state is StatePostPesananWaiting){
                    return Center(child: CircularProgressIndicator(backgroundColor: Colors.red,));
                  }
                  if(state is StatePostPesananFailed){
                    return buttonBayar(size);
                  }
                  return buttonBayar(size);
                },
              ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container buttonBayar(Size size) {
    return Container(
            width: size.width,
            child: ElevatedButton(
              onPressed: (){
                ModelRiwayatPesanan dataPesanan = ModelRiwayatPesanan(idKeranjang: widget.dataBarang.idKeranjang ,metodePembayaran: idMetodePembayaran, alamat: alamatPengiriman, idBarang: widget.dataBarang.idBarang,namaBarang: widget.dataBarang.namaBarang,qty: qty, harga: (int.parse(widget.dataBarang.diskon) == 0)?int.parse(widget.dataBarang.harga):int.parse(widget.dataBarang.hargaPromo), totalHarga: total_hargaBarang+10000); 
                blocPesanan..add(EventPostPesanan(data: dataPesanan));
              },
              child: Text("Bayar dengan "+namaMetodePembayaran.toString(), style: TextStyle(color: colorGrey),),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(contentColor)
              ),
            )

          );
  }
}
