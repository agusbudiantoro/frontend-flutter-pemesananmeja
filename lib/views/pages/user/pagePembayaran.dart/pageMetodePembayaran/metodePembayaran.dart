import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/metodePembayaran/metodePembayaran.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/metode_pembayaran_bloc/metode_pembayaran_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

class PilihMetodePembayaran extends StatefulWidget {

  @override
  _PilihMetodePembayaranState createState() => _PilihMetodePembayaranState();
}

class _PilihMetodePembayaranState extends State<PilihMetodePembayaran> {
  MetodePembayaranBloc bloc = MetodePembayaranBloc();
  ValuesMetodePembayaranModel tampungData = ValuesMetodePembayaranModel();
  int _value=0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc..add(EventGetMetodePembayaran());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: contentColor,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: colorGrey,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Metode Pembayaran",
                    style: TextStyle(fontSize: 14, color: colorGrey),
                  ),
                ),
                IconButton(
                    icon: Icon(
                      Icons.send,
                      color: colorGrey,
                    ),
                    onPressed: () {
                      Navigator.pop(context, tampungData);
                    }),
              ],
            ),
          )
        ],
      ),
      body: BlocBuilder<MetodePembayaranBloc, MetodePembayaranState>(
        bloc: bloc,
        builder: (context, state) {
          print(state);
          if(state is StateMetodePembayaranSukses){
            return pilihMetodePembayaran(state.data, size);
          }
          if(state is StateMetodePembayaranWaiting){
            return Container(child: Center(child: CircularProgressIndicator(),),);
          }
          if(state is StateMetodePembayaranFailed){
            return Container(child:Center(child:Text("Gagal Ambil Data")));
          }
          return Container();
        },
      ),
    );
  }

  Container pilihMetodePembayaran(List<ValuesMetodePembayaranModel> data, Size size) {
    print(data[0].statusPilih);
    print(data[0].namaPembayaran);
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.black;
    }
    return Container(
      child: ListView.builder(
        itemCount: data.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context,i){
          return Card(
            child: Container(
              padding: EdgeInsets.all(5),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      alignment: Alignment.topLeft,
                      child: Radio(
                        value: data[i].idMetodePembayaran, 
                        groupValue: _value, 
                        onChanged: (value){
                          setState(() {
                            _value = value;
                            tampungData=data[i];
                          });
                        })
                    ),
                    Container(
                      color: Colors.white,
                      height: 50,
                      width: size.width/1.5,
                      child: new Image.network(
                        domain+'logo/' +
                            data[i].logo.toString(),
                        fit: BoxFit.contain,
                      ),
                    ),
                ],
              ),
            ),
          );
        }
        ),
    );
  }
}