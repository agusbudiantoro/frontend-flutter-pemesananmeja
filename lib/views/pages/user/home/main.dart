import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/pages/gantiPass/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/login/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/feed/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/historyPembelian/historyPembelian.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/home/contentHome.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/keranjang/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeUser extends StatefulWidget {
  @override
  _HomeUserState createState() => _HomeUserState();
}

class _HomeUserState extends State<HomeUser> {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  String email;
  String username;

  void getAkun()async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username= preferences.getString("username");
      email = preferences.getString("myEmail");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAkun();
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.blue,
            appBarTheme: AppBarTheme(
                backgroundColor: background1,
                iconTheme: IconThemeData(color: circlePurpleDark, size: 36))),
        home: Scaffold(
          key: scaffoldKey,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            elevation: 0,
            leading: Padding(
              padding: const EdgeInsets.only(left:16.0),
              child: IconButton(
                icon: Icon(Icons.grid_view),
                onPressed: (){
                  scaffoldKey.currentState.openDrawer();
                },
                ),
            ),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FeedBarangPage(
                              jenis: 3,
                            )));
                },
                iconSize: 36,
                icon: Icon(Icons.search)
                ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network("https://img1.pngdownload.id/20180626/ehy/kisspng-avatar-user-computer-icons-software-developer-5b327cc951ae22.8377289615300354013346.jpg", width: 36,)),
              )
            ],
          ),
          body: ContentHome(),
          drawer: Card(
            elevation: 8,
            child: new Drawer(
                  child: new ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 55),
                child: Container(
                    width: size.width,
                    alignment: Alignment.center,
                    // padding: EdgeInsets.only(left: 38, right: 20),
                    // child: SearchHome(
                    //   height: size.height / 16,
                    //   width: (size.width * 5) / 6,
                    // ),
                    child: Column(
                      children: [
                        ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: background1,
                              border: Border.all(width: 3, color: captionColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: CircleAvatar(
                              backgroundImage:
                                  CachedNetworkImageProvider("https://img1.pngdownload.id/20180626/ehy/kisspng-avatar-user-computer-icons-software-developer-5b327cc951ae22.8377289615300354013346.jpg"),
                              radius: size.width / 7,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(username.toString(), style: TextStyle(fontWeight: FontWeight.bold),),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Text("User", style: TextStyle(fontWeight: FontWeight.bold),),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 20),
                        child: Text(email.toString(), style: TextStyle(fontWeight: FontWeight.normal),),
                      )
                      ],
                    )
                    ),
                decoration: BoxDecoration(
                  color: background1,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10,right:10),
                height: size.height,
                color: background1,
                child: Column(
                  children: [
                    menuItem("Keranjang",1),
                    menuItem("Pembayaran",2),
                    menuItem("Ganti Password",3),
                    Container(
                    width: size.width,
                    child: TextButton(
                      style: ButtonStyle(
                        overlayColor: MaterialStateProperty.all(captionColor)
                      ),
                      onPressed: (){
                        hapusPrefer();
                        return Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>Login()), (Route<dynamic> route) => false);
                      },
                      child: Text("Keluar", style: TextStyle(color:circlePurpleDark),),
                    ),
                  ),
                  ],
                ),
              ),
            ],
                  ),
                ),
          ),
        ));
  }

  void hapusPrefer()async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove("username");
      await preferences.remove("token");
      await preferences.remove("myId");
      await preferences.remove("myRole");
      await preferences.remove('myFoto');
      await preferences.remove("noHp");
      await preferences.remove("myAlamat");
      await preferences.remove("myEmail");
  }

  Widget menuItem(String name,int page) {
    return TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.all(captionColor)
      ),
      onPressed: (){
        if(page == 1){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>BackgroundKeranjang()));
        }
        if(page == 2){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>BackgroundHistori()));
        }
        if(page == 3){
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>PageGantiPassword()));
        }
      },
      child: Column(
        children: <Widget>[
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(name, style: TextStyle(color: colorGrey, fontWeight: FontWeight.bold),),
              Icon(
                Icons.keyboard_arrow_right,
                color: colorGrey,
              )
            ],
          ),
          SizedBox(height: 10),
          Divider(
            height: 0.5,
            color: colorGrey,
          )
        ],
      ),
    );
  }
}
