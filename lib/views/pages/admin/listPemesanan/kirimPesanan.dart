import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanan_bloc/pesanan_bloc.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/metodePengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/rincianHarga.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/alamatPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/detailbarang.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/historyStatusPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/informasiPesanan.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/noPesanan.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/noTransaksi.dart';

import 'del.dart';
import 'main.dart';

class KirimPesanan extends StatefulWidget {
  final ValueGetPemesanan dataPesanan;
  KirimPesanan({this.dataPesanan});
  @override
  _KirimPesananState createState() => _KirimPesananState();
}

class _KirimPesananState extends State<KirimPesanan> {
  PesananBloc blocPesan = PesananBloc();

  myDialog(ValueGetPemesanan data,BuildContext context){
    showDialog(
        context: context,
        builder: (BuildContext context) => CustomDialogPesanan(clickCallback: (){
          hapusPesanan(data);
        },),
      );
  }

  void hapusPesanan(ValueGetPemesanan data){
    blocPesan..add(EventDeletePesanan(data: data));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: captionColor,
      appBar: AppBar(
        backgroundColor: contentColor,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: colorGrey,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Detail Pembelian",
                    style: TextStyle(fontSize: 14, color: colorGrey),
                  ),
                ),
                BlocListener<PesananBloc, PesananState>(
                  bloc:blocPesan,
                  listener: (context, state) {
                    // TODO: implement listener
                    if(state is StateDeletePesananSukses){
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>DaftarPesanan()));
                    }
                  },
                  child: Container(
                    child: BlocBuilder<PesananBloc, PesananState>(
                      bloc:blocPesan,
                      builder: (context, state) {
                        if(state is StateDeletePesananSukses){
                          return buttonDelete(context);
                        }
                        if(state is StateDeletePesananWaiting){
                          return Container(child: Center(child: CircularProgressIndicator(),),);
                        }
                        if(state is StateDeleteFailed){
                          return buttonDelete(context);
                        }
                        return buttonDelete(context);
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      body: Container(
        color: Colors.grey[350],
        child: ListView(
          children: [
            SizedBox(
              height: 10,
            ),
            PageNoPesanan(noPesanan: widget.dataPesanan.noRiwayatPesanan.toString(),),
            SizedBox(
              height: 10,
            ),
            WidgetInformasiPesanan(width: size.width,height: size.height, status: widget.dataPesanan.status,statusCode: widget.dataPesanan.statusCodePembayaran,waktuPembayaran: widget.dataPesanan.waktuPembayaran,namaPembayaran: widget.dataPesanan.namaMetodePembayaran,logoPembaayaran: widget.dataPesanan.logoMetodePembayaran,totalHarga: widget.dataPesanan.totalHarga.toString(),),
            SizedBox(
              height: 10,
            ),
            WidgetAlamatPem(width: size.width,height: size.height,alamat: widget.dataPesanan.alamat.toString(),),
            SizedBox(
              height: 10,
            ),
            PageNoTransaksi(noTransaksi: widget.dataPesanan.midtransIdPemesanan,width: size.width,),
            WidgetHistoryPengiriman(width: size.width,height: size.height,statusPengiriman: widget.dataPesanan.statusPengiriman,),
            WidgetDaftarBelanjaPembelian(height: size.height,width: size.width,dataBarang: BarangModel(stok: widget.dataPesanan.qty, qty: widget.dataPesanan.qty,totalHarga: widget.dataPesanan.totalHarga, harga: widget.dataPesanan.harga.toString(),hargaPromo: widget.dataPesanan.harga.toString(),namaBarang: widget.dataPesanan.namaBarang,image: widget.dataPesanan.image,),),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(8),
        alignment: Alignment.centerLeft,
        height: size.height/12,
        color: Colors.white,
        child: Column(
          children: [
            (widget.dataPesanan.statusPengiriman == 1)?BlocListener<PesananBloc, PesananState>(
                  bloc:blocPesan,
                  listener: (context, state) {
                    // TODO: implement listener
                    if(state is StatePutStatusPesananSukses){
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>DaftarPesanan()));
                    }
                  },
                  child: Container(
                    child: BlocBuilder<PesananBloc, PesananState>(
                      bloc:blocPesan,
                      builder: (context, state) {
                        if(state is StatePutStatusPesananSukses){
                          return buttonKirim(size);
                        }
                        if(state is StatePutStatusPesananWaiting){
                          return Center(child: CircularProgressIndicator(backgroundColor: Colors.red,));
                        }
                        if(state is StatePutStatusFailed){
                          return buttonKirim(size);
                        }
                        return buttonKirim(size);
                      },
                    ),
                  ),
                ):Container()
          ],
        ),
      ),
    );
  }

  Container buttonKirim(Size size) {
    return Container(
            width: size.width,
            child: ElevatedButton(
              onPressed: (){
                return blocPesan..add(EventPutStatusPesanan(data: ValueGetPemesanan(noRiwayatPesanan: widget.dataPesanan.noRiwayatPesanan,statusPengiriman: widget.dataPesanan.statusPengiriman)));
              },
              child: Text("Kirim Barang", style: TextStyle(color: colorGrey),),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(contentColor)
              ),
            )

          );
  }

  IconButton buttonDelete(BuildContext context) {
    return IconButton(
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    return myDialog(ValueGetPemesanan(noRiwayatPesanan: widget.dataPesanan.noRiwayatPesanan, idBarang: widget.dataPesanan.idBarang, qty: widget.dataPesanan.qty), context);
                  });
  }
}
