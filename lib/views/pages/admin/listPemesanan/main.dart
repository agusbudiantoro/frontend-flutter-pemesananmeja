import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanan_bloc/pesanan_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';
import 'package:flutter_jamu_tujuh/views/utils/data/content.dart';

import 'kirimPesanan.dart';


class DaftarPesanan extends StatefulWidget {
  final int jenis;
  DaftarPesanan({this.jenis});
  @override
  _DaftarPesananState createState() => _DaftarPesananState();
}

class _DaftarPesananState extends State<DaftarPesanan> {
  bool isChecked = false;
  List<BarangModel> listKer;
  PesananBloc blocPesanan = PesananBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listKer = dataBarangKeranjang();
    blocPesanan..add(EventGetAllPemesanan());
  }

  Future<void> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return blocPesanan..add(EventGetAllPemesanan());
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: contentColor,
          iconTheme: IconThemeData(
            color: colorGrey, //change your color here
          ),
          title: Center(
              child: Text(
            "Daftar Pesanan",
            style: TextStyle(color: colorGrey),
          )),
        ),
        body: BlocBuilder<PesananBloc, PesananState>(
          bloc: blocPesanan,
          builder: (context, state) {
            if(state is StateGetAllPesananSukses){
              if (state.data != null) {
                    return listData(size, state.data);
                  } else {
                    return Container(
                      child: Center(
                        child: Text("data kosong"),
                      ),
                    );
                  }
            }
            if(state is StateGetAllPesananWaiting){
              return Container(child:Center(child:CircularProgressIndicator()));
            }
            if(state is StateGetAllPesananFailed){
              return Container(child:Center(child: CircularProgressIndicator(),));
            }
            return Container(child: Center(child: Text("Initial State"),),);
          },
        )
        );
  }

  RefreshIndicator listData(Size size, List<ValueGetPemesanan> dataHistory) {
    return RefreshIndicator(
        onRefresh: reffreshIn,
        child: ListView.separated(
          separatorBuilder: (context, position) {
            return SizedBox(height: 15,);
          },
          padding: EdgeInsets.all(10),
          itemCount: dataHistory.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int i) {
            return GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>KirimPesanan(dataPesanan: dataHistory[i])));
              },
                  child: Container(
                    decoration: BoxDecoration(
                      color: contentColor,
                      borderRadius: BorderRadius.circular(20)
                    ),
                  padding: EdgeInsets.only(left:10),
                  height: size.height / 7,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20)
                      ),
                      padding: EdgeInsets.only(left:5,right:5),
                              height: size.height/9,
                              width: size.width/5,
                              child: new Image.network(
                                domain+'gambar/'+dataHistory[i].image.toString(),
                                fit: BoxFit.contain,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 25, left: 10,right:10),
                              alignment: Alignment.topLeft,
                              width: size.width / 3,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.topLeft,
                                    width: size.width / 1.8,
                                    child: new Text(
                                      dataHistory[i].namaBarang.toString(),
                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 17),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20, left: 10,right:10),
                              alignment: Alignment.center,
                              width: size.width / 4,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.topLeft,
                                    width: size.width / 1.8,
                                    child: new Text(
                                      "IDR "+dataHistory[i].totalHarga.toString(),
                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(top:10),
                                    alignment: Alignment.topLeft,
                                    width: size.width / 1.8,
                                    child: new Text(
                                      (dataHistory[i].statusPengiriman == 0)?"Menunggu Pembayaran":(dataHistory[i].statusPengiriman == 1)?"Barang Yang Harus Dikirim":(dataHistory[i].statusPengiriman == 2)?"Barang Sedang Dikirm":(dataHistory[i].statusPengiriman == 3)?"Barang Sudah Sampai":"",
                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 12),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                  ],
                  )
                ),
            );
          },
        ),
      );
  }
}
