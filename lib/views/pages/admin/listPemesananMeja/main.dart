import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/jadwalbookmeja/main.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanLapangan_bloc/pesanlapangan_bloc.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/dialog_delete/dialog_delete.dart';

class ListPemesananMeja extends StatefulWidget {

  @override
  _ListPemesananMejaState createState() => _ListPemesananMejaState();
}

class _ListPemesananMejaState extends State<ListPemesananMeja> {
  PesanlapanganBloc blocPesanLapangan = PesanlapanganBloc();
  PesanlapanganBloc DelblocPesanLapangan = PesanlapanganBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    blocPesanLapangan..add(EventAllGetPesanan());
  }
  
  @override
  Widget build(BuildContext context) {
    var size=MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("List Pemesanan Meja"),
        elevation: 0,
      ),
      body: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
                    bloc: blocPesanLapangan,
                    builder: (context, state) {
                      print(state);
                      if(state is StateGetPesanLapanganAllSukses){
                        return ListViewChild(size, state.data);
                      }
                      if(state is StateGetPesanLapanganAllWaiting){
                        return Container(child: Center(child: CircularProgressIndicator(),),);
                      }
                      if(state is StateGetPesanLapanganAllFailed){
                        return Container(child: Center(child: Text("Gagal Memuat Data"),),);
                      }
                      return Container();
                    },
                  ),
    );
  }

  ListView ListViewChild(Size size, List<ModelListJadwal> data) {
    return ListView.builder(
              shrinkWrap: true,
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: data.length,
              itemBuilder: (BuildContext context, int i){
                return Container(
                  margin: EdgeInsets.all(5),
                  height: size.height/5,
                  width: size.width,
                  decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 2),
                          color: colorGrey, 
                          borderRadius: BorderRadius.circular(20)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Container(
                                  margin: EdgeInsets.all(15),
                                  // color: Colors.transparent,
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Text(data[i].namaTim, style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.normal)))),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Tanggal : "+data[i].tanggalPemesanan.toString(), style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal))),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Waktu : "+data[i].jam.toString(), style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal))),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text("Meja : "+data[i].nomorMeja.toString(), style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal))),
                                        )
                                    ],
                                  ),
                                )),
                                Expanded(
                                flex: 1,
                                child: BlocListener<PesanlapanganBloc, PesanlapanganState>(
                                  bloc: DelblocPesanLapangan,
                                  listener: (context, state) {
                                    if(state is StateDelPesanLapanganSukses){
                                      blocPesanLapangan..add(EventAllGetPesanan());
                                    }
                                    // TODO: implement listener
                                  },
                                  child: Container(
                                    child: BlocBuilder<PesanlapanganBloc, PesanlapanganState>(
                                      bloc: DelblocPesanLapangan,
                                      builder: (context, state) {
                                        if(state is StateDelPesanLapanganSukses){
                                          return deleteGesture(data, i, size);
                                        }
                                        if(state is StateDelPesanLapanganFailed){
                                          return  deleteGesture(data, i, size);
                                        }
                                        if(state is StateDelPesanLapanganWaiting){
                                          return Container(child: Center(child: CircularProgressIndicator(),),);
                                        }
                                        return deleteGesture(data, i, size);
                                      },
                                    ),
                                  ),
                                )
                                )  
                            ],
                          ),
                );
              }
              );
  }

  GestureDetector deleteGesture(List<ModelListJadwal> data, int i, Size size) {
    return GestureDetector(
                                onTap: (){
                                  _clickCallBackDelete(data[i].id);
                                  // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>ListAgendaAdmin(id: data[i].idBidang,)));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                  // border: Border.all(color: blackMetalic, width: 2),
                                    color: Colors.red, 
                                    borderRadius: BorderRadius.only(topRight: Radius.circular(20))),
                                  child: Column(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Icon(Icons.delete, size: size.width/10,color: Colors.white,),
                                      )
                                    ],
                                  )
                                ),
                              );
  }

  Future<void> _clickCallBackDelete(int id) {
    return showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => DialogDelete(id:id,callback: callbackDelete,)
            );
  }

  callbackDelete(int id){
    print("delete");
    print(id);
    DelblocPesanLapangan..add(EventDeletePesanan(id: id));
  }
}