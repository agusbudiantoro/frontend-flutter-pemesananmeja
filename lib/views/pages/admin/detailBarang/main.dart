import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

import 'content.dart';

class DetailProdukAdmin extends StatefulWidget {
  final String images;
  final String harga;
  final String hargaPromo;
  final String diskon;
  final String namaBarang;
  final String deskripsi;
  final int qty;
  final int idBarang;

  DetailProdukAdmin({this.idBarang,this.images, this.diskon, this.harga, this.hargaPromo, this.namaBarang, this.deskripsi, this.qty});
  @override
  _DetailProdukAdminState createState() => _DetailProdukAdminState();
}

class _DetailProdukAdminState extends State<DetailProdukAdmin> {
  @override
  void initState() { 
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background1,
      body: BackgroundDetailProdukAdmin(idBarang: widget.idBarang,images: widget.images,namaBarang: widget.namaBarang,harga: widget.harga,deskripsi: widget.deskripsi,diskon: widget.diskon,qty: widget.qty,hargaPromo: widget.hargaPromo,),
    );
  }
}