import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

class CustomDialog extends StatefulWidget {
  final VoidCallback clickCallback;

  CustomDialog({this.clickCallback});

  @override
  _CustomDialogState createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {
  bool statusDel=false;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  closeDialog(BuildContext context){
    return Navigator.pop(context);
  }

  dialogContent(BuildContext context) {
  return Container(
      decoration: new BoxDecoration(
        color: colorGrey,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisSize: MainAxisSize.min, // To make the card compact
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(padding: EdgeInsets.all(16), alignment: Alignment.centerRight, child: Icon(Icons.close, color: Colors.red)),
          ),
          Icon(Icons.warning_amber,size: 80,color: Colors.red,),
          SizedBox(height: 24),
          SizedBox(height: 24),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Text("Apakah and ingin menghapus barang ini?",style: TextStyle(color: background1)),
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              (statusDel == false)?GestureDetector(
              onTap: (){
              widget.clickCallback();
              setState(() {
                statusDel=true;
              });
              return Timer(Duration(seconds: 3), closeDialog(context));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width/3.5,
                  margin: EdgeInsets.all(16),
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  decoration: new BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(24),
                  ),
                  alignment: Alignment.center,
                  child: Text("Hapus", style: TextStyle(color: Colors.white),),
                ),
              ):Center(child: CircularProgressIndicator(),),
              GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(
              width: MediaQuery.of(context).size.width/3.5,
              margin: EdgeInsets.all(16),
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              decoration: new BoxDecoration(
                color: circlePurpleDark,
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(24),
              ),
              alignment: Alignment.center,
              child: Text("Tutup", style: TextStyle(color: Colors.white)),
            ),
          )
            ],
          ),
          
        ],
      ));
}
}