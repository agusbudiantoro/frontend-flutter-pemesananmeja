import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_barang/barang_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_kategori/kategori_bloc.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/kelolaBarangAdmin/field.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/alamatPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/historyStatusPengiriman.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/informasiPesanan.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/noPesanan.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/pembayaran/detailPembelian/noTransaksi.dart';
import 'package:image_picker/image_picker.dart';

import 'indexListBarang.dart';

class AddBarang extends StatefulWidget {

   @override
  _AddBarangState createState() => _AddBarangState();
}

class _AddBarangState extends State<AddBarang> {
  File imageFile;
  KategoriBloc blocKategori = KategoriBloc();
  BarangBloc blocBarang = BarangBloc();
  String myValue='Makanan';
  int idKategori=1;
  TextEditingController namaBarang = TextEditingController();
  TextEditingController qty = TextEditingController();
  TextEditingController harga = TextEditingController();
  TextEditingController promo = TextEditingController();
  TextEditingController deskripsi = TextEditingController();
  TextEditingController hargaPromo = TextEditingController();
  TextEditingController kategori = TextEditingController();

  @override
  void initState() { 
    super.initState();
    blocKategori..add(GetKategoriEvent());
  }

  _openGalery() async {
    var picture = await ImagePicker().getImage(source: ImageSource.gallery);
    setState(() {    
      if(picture.path != null){
        imageFile = File(picture.path);
      }
      
    });

  }

  void _onChangedCallBack() {
    setState(() {
      if(int.parse(promo.text) != 0){
        hargaPromo.text = (int.parse(harga.text)-((int.parse(promo.text)*int.parse(harga.text))/100)).toString();
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: contentColor,
      appBar: AppBar(
        backgroundColor: contentColor,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: colorGrey,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Tambah Barang",
                    style: TextStyle(fontSize: 14, color: colorGrey),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(top:10),
                        height: MediaQuery.of(context).size.height / 7,
                        child: (imageFile == null)?IconButton(icon: Icon(Icons.add_photo_alternate_outlined), onPressed: (){
                          return _openGalery();
                        }):
                        Image.file(
                          imageFile,
                          width: 110,
                          height: 50,
                        )),
                  ),
                  Padding(padding: EdgeInsets.all(6.0)),
                  Flexible(
                      flex: 2,
                      child: TextField(
                        controller: namaBarang,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(hintText: "Nama Barang"),
                      )),
                ],
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: TexFieldCustom(containerField: "Qty",isi: qty,readOnly: false,typeNumber: true,),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: TexFieldCustom(containerField: "Harga",isi: harga,readOnly: false,typeNumber: true,),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: TexFieldCustom(containerField: "Promo *isi '0' jika tidak ada promo",isi: promo,readOnly: false,typeNumber: true,onChangeCall: ()=>_onChangedCallBack(),),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: TexFieldCustom(containerField: "Deskripsi",isi: deskripsi, readOnly: false,typeNumber: false,),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: TexFieldCustom(containerField: "Harga Promo",isi: hargaPromo, readOnly: (promo.text == '')?true:(promo.text.toString() == '0')?true:false,typeNumber: true,),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: Container(
                margin: EdgeInsets.all(5),
                child: BlocBuilder<KategoriBloc, KategoriState>(
                  bloc:blocKategori,
                  builder: (context, state) {
                    if(state is KategoriStateSukses){
                      return dripDownButton(state);
                    }
                    if(state is KategoriStateLoading){
                      return Center(child: CircularProgressIndicator(),);
                    }
                    if(state is KategoriStateFailed){
                      return Container(child: Center(child: Text("Gagal Ambil Data Kategori"),),);
                    }
                    return Container();
                  },
                )
              )
              )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(8),
        alignment: Alignment.centerLeft,
        height: size.height/12,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              width: size.width,
              child: BlocListener<BarangBloc, BarangState>(
                bloc: blocBarang,
                listener: (context, state) {
                  if(state is StatePostBarangSukses){
                    Navigator.pop(context);
                    // Navigator.push((context), MaterialPageRoute(builder:(context)=>BackgroundMenuAdmin(page: 2,)));
                  }
                },
                child: Container(
                  child: BlocBuilder<BarangBloc, BarangState>(
                bloc: blocBarang,
                builder: (context, state) {
                  if(state is StatePostBarangSukses){
                    return buttonTambah();
                  }if(state is StatePostBarangWaiting){
                    return Center(child: CircularProgressIndicator(backgroundColor: Colors.red,));
                  }if(state is StatePostBarangFailed){
                    return buttonTambah();
                  }
                  return buttonTambah();
                },
              ),
                ),
              )
              
            )
          ],
        ),
      ),
    );
  }

  ElevatedButton buttonTambah() {
    return ElevatedButton(
              onPressed: (){
                BarangModel postData = BarangModel(deskripsi: deskripsi.text, harga: harga.text, namaBarang: namaBarang.text,qty: int.parse(qty.text),diskon: promo.text,kategori: idKategori.toString(), path: imageFile, hargaPromo: hargaPromo.text.toString());
                blocBarang..add(EventPostBarang(data: postData));
              },
              child: Text("Tambah Barang", style: TextStyle(color: Colors.white),),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.red)
              ),
            );
  }

  DropdownButton<String> dripDownButton(KategoriStateSukses data) {
    List<ValuesKategori> dataKategori = data.myData;
    List<String> isi =[];
    dataKategori.forEach((element) {
      isi.add(element.namaKategori);
    });
    
    return DropdownButton(
                value: myValue,
                icon: Icon(Icons.keyboard_arrow_down),
                items:isi.map((String items) {
                     return DropdownMenuItem(
                         value: items,
                         child: Text(items.toString())
                     );
                }
                ).toList(),
                onChanged: (String newValue){
                setState(() {
                  myValue = newValue;
                  dataKategori.forEach((element) {
                    if(element.namaKategori == myValue){
                      idKategori=element.id;
                    }
                  });
                });
              },
              );
  }
}
