import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_barang/barang_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_kategori/kategori_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/pages/admin/detailBarang/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmermiddle2Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/middle2Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/data/content.dart';
import 'dart:math';

import 'addBarang.dart';
import 'del.dart';
import 'editBarang.dart';

class ListBarang extends StatefulWidget {
  String searchBarang;
  final int jenis;

  ListBarang({this.jenis, this.searchBarang});
  @override
  _ListBarangState createState() => _ListBarangState(jenis: this.jenis);
}

class _ListBarangState extends State<ListBarang> with SingleTickerProviderStateMixin{
  List<ValuesKategori> listKat;
  List<BarangModel> listIsi;
  final int jenis;
  BarangBloc blocBarang = BarangBloc();
  KategoriBloc blocKategori = KategoriBloc();
  ValuesKategori valueKat=ValuesKategori(id: 0, namaKategori: "Semua");
  String cariBarang="";
  int toggle = 0;
  AnimationController _con;
  TextEditingController _textEditingController;

  _ListBarangState({this.jenis});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listKat = dataKategori();
    if (jenis == 1) {
      listIsi = dataBarangRek();
    } else if (jenis == 2) {
      listIsi = dataBarangPro();
    } else if (jenis == 3) {
      listIsi = dataBarangRek() + dataBarangPro();
    }
    blocBarang..add(EventGetBarang());
    blocKategori..add(GetKategoriEvent());
    _textEditingController = TextEditingController();
    _con = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 375),
    );
  }

  myDialog(id,BuildContext context){
    showDialog(
        context: context,
        builder: (BuildContext context) => CustomDialog(clickCallback: (){
          hapusBarang(id);
        },),
      );
  }
  doSomethingKetgori(ValuesKategori data) { 
    setState(() {
      valueKat=(data != null)?data:ValuesKategori(id: 0, namaKategori: "Semua");
    });
    print(valueKat.namaKategori);
    print(valueKat.id);
  }

  void hapusBarang(id){
    BarangModel myData = BarangModel(idBarang: id);
    blocBarang..add(EventDelBarang(data: myData));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
            color: background1,
            padding: EdgeInsets.only(top:size.height/15, right: 10, left: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                child: IconButton(
                  color: Colors.white,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back, color: colorGrey,),
                ),
              ),
                Container(
            height:size.height/18,
            width: size.width/1.5,
            alignment: Alignment(1.0, 0.0),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 375),
              height: 48.0,
              width: (toggle == 0) ? 48.0 : 250.0,
              curve: Curves.easeOut,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    spreadRadius: -1.0,
                    blurRadius: 1.0,
                    offset: Offset(0.0, 1.0),
                  ),
                ],
              ),
              child: Stack(
                children: [
                  AnimatedPositioned(
                    duration: Duration(milliseconds: 375),
                    left: (toggle == 0) ? 20.0 : 40.0,
                    curve: Curves.easeOut,
                    top: 11.0,
                    child: AnimatedOpacity(
                      opacity: (toggle == 0) ? 0.0 : 1.0,
                      duration: Duration(milliseconds: 200),
                      child: Container(
                        height: 23.0,
                        width: 180.0,
                        child: TextField(
                          onChanged: (val){
                            setState(() {
                              cariBarang=val.toString();
                            });
                          },
                          autofocus: (widget.jenis == 3)?true:false,
                          cursorRadius: Radius.circular(10.0),
                          cursorWidth: 2.0,
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            labelText: 'Pencarian',
                            labelStyle: TextStyle(
                              color: Color(0xff5B5B5B),
                              fontSize: 17.0,
                              fontWeight: FontWeight.w500,
                            ),
                            alignLabelWithHint: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: BorderSide.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Material(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30.0),
                    child: IconButton(
                      splashRadius: 19.0,
                      icon: Icon(Icons.search_sharp),
                      onPressed: () {
                        setState(
                          () {
                            if (toggle == 0) {
                              toggle = 1;
                              _con.forward();
                            } else {
                              toggle = 0;
                              _textEditingController.clear();
                              _con.reverse();
                            }
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
              Container(
                child: IconButton(
                  color: colorGrey,
                  onPressed: () {
                    Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddBarang()));
                  },
                  icon: Icon(Icons.add_box_outlined),
                ),
              )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left:20,top:5, bottom: 5),
            width: size.width,
            color: background1,
            child: Text("Manajemen Barang", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
          ),
        Container(
          color: background1,
            padding: EdgeInsets.symmetric(horizontal: 20,),
            width: size.width,
            height: size.height / 15,
            child: BlocBuilder<KategoriBloc, KategoriState>(
                            bloc:blocKategori,
                            builder: (context, state) {
                              if(state is KategoriStateSukses){
                                return Middle2Container(listKat: state.myData,callback: doSomethingKetgori);
                              }
                              if(state is KategoriStateLoading){
                                return ShimmerMiddle2Container();
                              }
                              if(state is KategoriStateFailed){
                                return Container(child: Center(child: Text("Gagal Ambil Data Kategori"),),);
                              }
                              return Container();
                            },
                          ),
          ),
        BlocListener<BarangBloc, BarangState>(
          bloc: blocBarang,
          listener: (context, state) {
            if(state is StateDelBarangSukses){
              return blocBarang..add(EventGetBarang());
            }
          },
          child: Container(
            child: BlocBuilder<BarangBloc, BarangState>(
            bloc: blocBarang,
            builder: (context, state) {
              if(state is StateGetBarangSukses){
                return listBodyFeed(size, state.listData);
              }
              if(state is StateGetBarangWaiting){
                return Center(child:CircularProgressIndicator());
              }
              if(state is StateGetBarangFailed){
                return Container(child: Center(child: Text("Gagal Mengambil Data"),),);
              }
              if(state is StateDelBarangFailed){
                return Container(child: Center(child: Text("Gagal Hapus Data"),),);
              }
              if(state is StateDelBarangWaiting){
                return Center(child:CircularProgressIndicator());
              }
              return Container();
            },
          ),
          ),
        )
      ],
    );
  }

  Future<void> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    blocBarang..add(EventGetBarang());
    blocKategori..add(GetKategoriEvent());
  }



  Expanded listBodyFeed(Size size,List<ValuesListBarang> data) {
    List<ValuesListBarang> listData = List<ValuesListBarang>();
      if(cariBarang.toString().length >0){
      if(valueKat.id == 0){
      listData = data.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase())).toList();
      } else if(valueKat.id != 0){
      listData = data.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase()) && i.kategori == valueKat.id).toList();
      }
      } else if(cariBarang.toString().length == 0){
      if(valueKat.id == 0){
      listData = data;
      } else if(valueKat.id != 0){
      listData = data.where((i) => i.kategori == valueKat.id).toList();
      }
      }
    return Expanded(
          child: RefreshIndicator(
        color: circlePurpleDark,
        backgroundColor: captionColor,
          onRefresh: reffreshIn,
        child: ListView.separated(
          separatorBuilder: (context, position) {
            return SizedBox(height: 15,);
          },
          scrollDirection: Axis.vertical,
          itemCount: listData.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailProdukAdmin(
                              idBarang: listData[index].idBarang,
                              images: listData[index].gambar.toString(),
                              hargaPromo: listData[index].hargaPromo.toString(),
                              namaBarang: listData[index].namaBarang.toString(),
                              harga: listData[index].harga.toString(),
                              deskripsi: listData[index].deskripsi.toString(),
                              qty: listData[index].qty,
                              diskon: listData[index].promo.toString(),
                            )));
              },
              child: Padding(
                padding: EdgeInsets.only(left:20,right:20),
                child: Container(
                  height: size.height/5.4,
                  decoration: BoxDecoration(
                    color: contentColor,
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        flex:2,
                        child: Padding(
                          padding: const EdgeInsets.only(left:15.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: Container(
                              padding: EdgeInsets.all(10),
                                    color:background1,
                                    width: size.width/3.3,
                                    height: size.height / 6,
                                    child: new Image.network(
                                      domain+'gambar/'+listData[index].gambar.toString(),
                                      fit: BoxFit.contain,
                                    )),
                          ),
                        ),
                      ),
                      Expanded(
                        flex:2,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          // width: size.width/3.2,
                          padding: EdgeInsets.symmetric(vertical: 20),
                          child: Column(
                            children: [
                              Expanded(
                                flex:2,
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(top:5,bottom: 5,left: 10),
                                  child: Text(listData[index].namaBarang.toString(),style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)),
                                ),
                              ),
                              (listData[index].promo != 0)?Expanded(
                                // flex:2,
                                child: Container(
                                  padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right:10),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    listData[index].promo.toString()+"%",
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold),
                                  )),
                              ):Container(),
                                 (listData[index].promo != 0)
                                ? Expanded(
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(top:5,bottom: 5,left: 10),
                                      child: Text(
                                          "IDR "+listData[index].hargaPromo.toString(),style: TextStyle(color: circlePurpleDark, fontWeight: FontWeight.w600)),
                                    ),
                                )
                                : Expanded(
                                  child: Container(
                                        alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right: 10),
                                          child: Text("IDR "+listData[index].harga.toString()),
                                        ),
                                ),
                                      (listData[index].promo == 0)
                                ? Expanded(
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right: 10),
                                    ),
                                )
                                : Expanded(
                                  child: Container(
                                    alignment: Alignment.centerLeft,
                                      padding: EdgeInsets.only(top:5,bottom: 5,left: 10, right: 10),
                                      child: Text(
                                        "IDR "+listData[index].harga.toString(),
                                        style: TextStyle(
                                          color: Colors.red,
                                            fontSize: 12,
                                            decoration:
                                                TextDecoration.lineThrough),
                                      ),
                                    ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex:2,
                        child: Container(
                          width: size.width/4,
                          height: size.height/5.4,
                        padding: EdgeInsets.only(top: 20),
                        child: Column(
                          children: [
                            TextButton(
                              style: ButtonStyle(
                                overlayColor: MaterialStateProperty.all(colorGrey)
                              ),
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>EditBarang(idBarang: listData[index].idBarang.toString(),namaBarang: listData[index].namaBarang,qty: listData[index].qty.toString(),harga: listData[index].harga.toString(),promo: listData[index].promo.toString(), hargaPromo: listData[index].hargaPromo.toString(),deskripsi: listData[index].deskripsi,kategori: listData[index].kategori.toString(),)));
                              }, 
                              child: Container(
                                padding: EdgeInsets.all(8),
                                alignment: Alignment.center,
                                height: 40,
                                width: size.width/4,
                                decoration: BoxDecoration(
                                  color: background1,
                                  borderRadius: BorderRadius.circular(88)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(Icons.edit_attributes_outlined, color: Colors.yellow),
                                    Text("Edit", style: TextStyle(color: circlePurpleDark),)
                                  ],
                                ),
                              )),
                              TextButton(
                              style: ButtonStyle(
                                overlayColor: MaterialStateProperty.all(colorGrey)
                              ),
                              onPressed: (){
                                myDialog(listData[index].idBarang, context);
                              }, 
                              child: Container(
                                padding: EdgeInsets.all(8),
                                alignment: Alignment.center,
                                height: 40,
                                width: size.width/4,
                                decoration: BoxDecoration(
                                  color: background1,
                                  borderRadius: BorderRadius.circular(88)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(Icons.delete_forever,color: Colors.redAccent),
                                    Text("Hapus", style: TextStyle(color: circlePurpleDark),)
                                  ],
                                ),
                              ))
                          ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ));
  }
}
