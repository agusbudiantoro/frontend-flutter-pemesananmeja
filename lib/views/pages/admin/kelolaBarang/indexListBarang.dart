import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

import 'addBarang.dart';
import 'listBarang.dart';


class IndexListBarang extends StatefulWidget {
  final int jenis;

  IndexListBarang({this.jenis});
  @override
  _IndexListBarangState createState() => _IndexListBarangState();
}

class _IndexListBarangState extends State<IndexListBarang> {
  TextEditingController cariBarang= TextEditingController();
  String setCariBarang="";

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background1,
      body: ListBarang(jenis: widget.jenis, searchBarang: setCariBarang.toString(),)
    );
  }
}
