import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerContentFeed extends StatefulWidget {

  @override
  _ShimmerContentFeedState createState() => _ShimmerContentFeedState();
}

class _ShimmerContentFeedState extends State<ShimmerContentFeed> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Expanded(
          child: Padding(
        padding: EdgeInsets.all(5),
        child: GridView.builder(
          scrollDirection: Axis.vertical,
          itemCount: 6,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Shimmer.fromColors(
              baseColor: colorGrey, 
              highlightColor: background1,
              child: Container(
                        // color: Colors.white,
                        child: Stack(
                          children: [
                            Container(
                                color: Colors.white,
                                width: size.width,
                                height: size.height / 6,
                                // child: new Image.network(
                                //   domain+'gambar/'+listData[index].gambar.toString(),
                                //   fit: BoxFit.contain,
                                // )
                                ),
                          ],
                        ),
                      ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 11,
            mainAxisSpacing: 11,
          ),
        ),
      ));
  }
}