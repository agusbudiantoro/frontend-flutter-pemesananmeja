import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerBottomContainer extends StatefulWidget {
  List<ValuesListBarang> listPromo;
  ShimmerBottomContainer({this.listPromo});

  @override
  _ShimmerBottomContainerState createState() => _ShimmerBottomContainerState();
}

class _ShimmerBottomContainerState extends State<ShimmerBottomContainer> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Shimmer.fromColors(
    baseColor: Colors.grey,
    highlightColor: Colors.white,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Spesial untuk Kamu", style: TextStyle(color: background1, fontWeight: FontWeight.bold, fontSize: 19),),
                  TextButton(
                    onPressed: (){
    
                  }, 
                  child: Text("Lihat Semua", style: TextStyle(color: Colors.red),))
                ],
              ),
            ),
            SizedBox(height: 5,),
            Card(
              elevation: 0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(24))),
              clipBehavior: Clip.antiAlias,
              child: Container(
                width: size.width,
                height: size.height/8,
                color: colorGrey,
                child: Stack(
                  children: [
                    Positioned(
                      bottom: -10,
                      right: -5,
                      child: Container(
                        decoration: BoxDecoration(
                            color: background1,
                            borderRadius: BorderRadius.circular(8)),
                        width: 45,
                        height: 45,
                        child: Icon(
                          Icons.add,
                          color: circlePurpleDark,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        // Container(
                        //   // color: Colors.red,
                        //   width: size.width/3.5,
                        //   height: (size.height / 4) / 2,
                        //   child: new Image.network(
                        //     domain+'gambar/' +
                        //         widget.listPromo[0].gambar.toString(),
                        //     fit: BoxFit.contain,
                        //   ),
                        // ),
                        // SizedBox(width: 5),
                        // Container(
                        //   width: size.width-(size.width/2),
                        //   // alignment: Alignment.centerLeft,
                        //   child: Column(
                        //     mainAxisAlignment: MainAxisAlignment.center,
                        //     children: [
                        //       Container(
                        //         alignment: Alignment.centerLeft,
                        //         child: Text(widget.listPromo[0].namaBarang.toString(), style: TextStyle(color: background1,fontSize: 17, fontWeight: FontWeight.bold),)),
                        //       SizedBox(height: 5,),
                        //       Container(
                        //         alignment: Alignment.centerLeft,
                        //         child: Text("IDR "+widget.listPromo[0].hargaPromo.toString(), style: TextStyle(color: captionColor, fontSize: 12),)),
                        //         SizedBox(height:5),
                        //         Container(
                        //         alignment: Alignment.centerLeft,
                        //         child: Text("Promo: "+widget.listPromo[0].promo.toString()+"%", style: TextStyle(color: captionColor, fontSize: 12),))
                        //     ],
                        //   ),
                        // )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}