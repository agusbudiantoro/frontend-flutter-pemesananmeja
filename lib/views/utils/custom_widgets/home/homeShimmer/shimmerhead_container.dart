import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/models/modelsdummy/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerHeadWidget extends StatelessWidget {
  List<ValuesListBarang> listRek;

  ShimmerHeadWidget({this.listRek});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height/4.5,
      child: Stack(children: [
        Positioned(
          top: size.height/50,
          left: 0,
          right: 0,
          child: Shimmer.fromColors(
            baseColor: Colors.grey, 
            highlightColor: Colors.white,
            child: Container(
              height: size.height/5,
              decoration: BoxDecoration(
                  color: circlePurpleDark,
                  borderRadius: BorderRadius.circular(36)),
            ),
          ),
        ),
        // Positioned(
        //   top: size.height/15,
        //   left: 0,
        //   right: 0,
        //   child: Text(
        //     listRek[1].namaBarang.toString(),
        //     style: TextStyle(
        //         color: Colors.grey.withOpacity(0.2), fontWeight: FontWeight.bold, fontSize: 80),
        //   ),
        // ),
        // Positioned(
        //   top: -size.height/40,
        //   left: -size.height/30,
        //   child: Container(
        //     width: size.width,
        //     // color: Colors.white,
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.spaceAround,
        //       children: [
        //         Image.network(domain+'gambar/'+listRek[1].gambar.toString(),
        //           height: size.height/4,
        //         ),
        //         Padding(
        //           padding: EdgeInsets.only(top: 10),
        //           child: Column(
        //             mainAxisAlignment: MainAxisAlignment.center,
        //             children: [
        //               Container(
        //                 padding: EdgeInsets.all(4),
        //                 decoration: BoxDecoration(
        //                     borderRadius: BorderRadius.circular(4),
        //                     color: colorGrey),
        //                 child: Text(
        //                   "New Product",
        //                   style: TextStyle(
        //                       color: captionColor,
        //                       fontWeight: FontWeight.bold,
        //                       fontSize: 12),
        //                 ),
        //               ),
        //               SizedBox(height: 8),
        //               Text(
        //                 listRek[1].namaBarang.toString(),
        //                 style: TextStyle(
        //                     color: background1,
        //                     fontWeight: FontWeight.w600,
        //                     fontSize: 16),
        //               ),
        //               SizedBox(height: 4),
        //               TextButton(
        //                   onPressed: () {},
        //                   child: Text(
        //                     "Beli Sekarang",
        //                     style: TextStyle(color: background1),
        //                   ),
        //                   style: TextButton.styleFrom(
        //                       backgroundColor: Colors.red,
        //                       primary: Colors.yellow,
        //                       padding:
        //                           EdgeInsets.symmetric(vertical: 4, horizontal: 12),
        //                       minimumSize: Size(5, 5)))
        //             ],
        //           ),
        //         )
        //       ],
        //     ),
        //   ),
        // )
      ]),
    );
  }
}
