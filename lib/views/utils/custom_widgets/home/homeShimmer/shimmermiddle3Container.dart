import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerMiddle3Container extends StatefulWidget {
  List<ValuesListBarang> listRek;
  ShimmerMiddle3Container({this.listRek});

  @override
  _ShimmerMiddle3ContainerState createState() => _ShimmerMiddle3ContainerState();
}

class _ShimmerMiddle3ContainerState extends State<ShimmerMiddle3Container> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Shimmer.fromColors(
    baseColor: Colors.grey,
    highlightColor: Colors.white,
      child: Container(
        width: size.width,
        height: size.height / 4.5,
        child: Row(
          children: [
            Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: 2,
                    scrollDirection: Axis.horizontal,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int i) {
                      return Row(
                        children: [
                          Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: contentColor,
                                  borderRadius: BorderRadius.all(Radius.circular(30)),
                                  ),
                              // padding: EdgeInsets.all(10),
                              height: size.height / 4,
                              width: size.width / 2.4,
                              child: Stack(
                                children: [
                                  // Positioned(
                                  //   bottom: -10,
                                  //   right: 0,
                                  //   child: Container(
                                  //     decoration: BoxDecoration(
                                  //       color: circlePurpleDark,
                                  //       borderRadius: BorderRadius.circular(8)
                                  //     ),
                                  //     width: 50,
                                  //     height: 50,
                                  //     child: Icon(Icons.add, color: background1,),
                                  //   ),
                                  // ),
                                  // Column(
                                  //   children: [
                                  //     Container(
                                  //       width: size.width,
                                  //       height: (size.height / 4) / 2,
                                  //       child: new Image.network(
                                  //         domain+'gambar/' +
                                  //             widget.listRek[i].gambar.toString(),
                                  //         fit: BoxFit.contain,
                                  //       ),
                                  //     ),
                                  //     SizedBox(height: 5,),
                                  //     Container(
                                  //       padding: EdgeInsets.all(10),
                                  //       alignment: Alignment.centerLeft,
                                  //       child: Text(widget.listRek[i].namaBarang.toString(), style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                                  //     ),
                                  //     Container(
                                  //       padding: EdgeInsets.only(left:10),
                                  //       alignment: Alignment.centerLeft,
                                  //       child: Text((widget.listRek[i].promo == 0)?"IDR "+widget.listRek[i].harga.toString():"IDR "+widget.listRek[i].hargaPromo.toString(), style: TextStyle(color: captionColor, fontWeight: FontWeight.w600),),
                                  //     )
                                  //   ],
                                  // ),
                                  // (widget.listRek[i].promo != 0)?Positioned(
                                  //   top: -20,
                                  //   right: -45,
                                  //   child: Container(
                                  //       alignment: Alignment.center,
                                  //       width: 100,
                                  //       height: 20,
                                  //       transform: Matrix4.rotationZ(0.8),
                                  //       color: Colors.red,
                                  //       child: Text(
                                  //         widget.listRek[i].promo.toString()+"%",
                                  //         style: TextStyle(
                                  //             color: Colors.white,
                                  //             fontWeight: FontWeight.bold),
                                  //       )),
                                  // ):Container(),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          )
                        ],
                      );
                    }))
          ],
        ),
      ),
    );
  }
}
