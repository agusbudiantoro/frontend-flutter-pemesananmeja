import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/feed/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/pesanMeja/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

class Middle1Container extends StatefulWidget {
  @override
  _Middle1ContainerState createState() => _Middle1ContainerState();
}

class _Middle1ContainerState extends State<Middle1Container> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Produk Kami",
                  style: TextStyle(
                      color: circlePurpleDark,
                      fontWeight: FontWeight.w600,
                      fontSize: 20),
                ),
                
                Row(
                  children: [
                    Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: contentColor,
                    borderRadius: BorderRadius.circular(8)
                  ),
                  child: IconButton(
                    focusColor: Colors.yellow,
                    color: circlePurpleDark,
                    onPressed: (){
                      Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PagePesanMeja()));
                    },
                    icon: Icon(Icons.chair_alt)),
                ),
                SizedBox(width: 5,),
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: contentColor,
                        borderRadius: BorderRadius.circular(8)
                      ),
                      child: IconButton(
                        focusColor: Colors.yellow,
                        color: circlePurpleDark,
                        onPressed: (){
                          Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => FeedBarangPage(
                                                    jenis: 1,
                                                  )));
                        },
                        icon: Icon(Icons.list)),
                    ),
                  ],
                )
              ],
            ),
          )
    );
  }
}
