import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_kategori/kategori_bloc.dart';

import '../../../colors.dart';
typedef GetKatgori = void Function(ValuesKategori);
class Middle2Container extends StatefulWidget {
  List<ValuesKategori> listKat;
  final GetKatgori callback;
  Middle2Container({this.listKat, this.callback});
  @override
  _Middle2ContainerState createState() =>
      _Middle2ContainerState(listKat: this.listKat);
}

class _Middle2ContainerState extends State<Middle2Container> {
  List<ValuesKategori> listKat;
  _Middle2ContainerState({this.listKat});
  int idKategori = 0;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(top: 10, bottom: 10),
        height: 50,
        // width: size.width - (size.width / 5.5),
        child: Row(
          children: [
            GestureDetector(
              child: (idKategori == 0)?Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 10,right: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4), color: contentColor),
                child: Text(
                  "Semua",
                  style: TextStyle(
                      color: circlePurpleDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ),
              ):Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 10,right: 10),
                child: Text(
                  "Semua",
                  style: TextStyle(
                      color: circlePurpleDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ),
              ),
              onTap: (){
                setState(() {
                  idKategori=0;
                  if(idKategori == 0){
                    widget.callback(ValuesKategori(id: 0,namaKategori: "Semua"));
                  }
                });
              },
            ),
            pilihKategori(listKat)
          ],
        ));
  }

  Flexible pilihKategori(List<ValuesKategori> data) {
    return Flexible(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: data.length,
        scrollDirection: Axis.horizontal,
        physics: AlwaysScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int i) {
          return GestureDetector(
              child: (idKategori == data[i].id)?Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left:10, right: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4), color: contentColor),
                child: Text(
                  data[i].namaKategori.toString(),
                  style: TextStyle(
                      color: circlePurpleDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ),
              ):
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 10,right: 10),
                child: Text(
                  data[i].namaKategori.toString(),
                  style: TextStyle(
                      color: circlePurpleDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 12),
                ),
              ),
              onTap: (){
                setState(() {
                  idKategori=data[i].id;
                  if(idKategori == data[i].id){
                    widget.callback(ValuesKategori(id: data[i].id,namaKategori: data[i].namaKategori));
                  }
                });
              },
            );
        },
      ),
    );
  }
}
