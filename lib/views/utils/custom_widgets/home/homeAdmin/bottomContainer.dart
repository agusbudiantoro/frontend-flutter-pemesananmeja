import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/feed/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
typedef getBarangPromo = void Function(BarangModel);
class BottomContainerAdmin extends StatefulWidget {
  final getBarangPromo callback;
  List<ValuesListBarang> listPromo;
  BottomContainerAdmin({this.listPromo, this.callback});

  @override
  _BottomContainerAdminState createState() => _BottomContainerAdminState();
}

class _BottomContainerAdminState extends State<BottomContainerAdmin> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left:10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Spesial untuk Kamu", style: TextStyle(color: background1, fontWeight: FontWeight.bold, fontSize: 19),),
              ],
            ),
          ),
          SizedBox(height: 15,),
          Card(
            elevation: 0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(24))),
            clipBehavior: Clip.antiAlias,
            child: Container(
              width: size.width,
              height: size.height/8,
              color: contentColor,
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        // color: Colors.red,
                        width: size.width/3.5,
                        height: (size.height / 4) / 2,
                        child: new Image.network(
                          domain+'gambar/' +
                              widget.listPromo[0].gambar.toString(),
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(width: 5),
                      Container(
                        width: size.width-(size.width/2),
                        // alignment: Alignment.centerLeft,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(widget.listPromo[0].namaBarang.toString(), style: TextStyle(color: colorGrey,fontSize: 17, fontWeight: FontWeight.bold),)),
                            SizedBox(height: 5,),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text("IDR "+widget.listPromo[0].hargaPromo.toString(), style: TextStyle(color: colorGrey, fontSize: 12),)),
                              SizedBox(height:5),
                              Container(
                              alignment: Alignment.centerLeft,
                              child: Text("Promo: "+widget.listPromo[0].promo.toString()+"%", style: TextStyle(color: colorGrey, fontSize: 12),))
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}