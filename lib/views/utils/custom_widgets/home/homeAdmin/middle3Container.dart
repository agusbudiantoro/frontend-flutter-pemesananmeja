import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/pages/admin/detailBarang/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/detailBarang/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
typedef GetBarangAll = void Function(BarangModel);
class Middle3ContainerAdmin extends StatefulWidget {
  List<ValuesListBarang> listRek;
  final GetBarangAll callback;
  Middle3ContainerAdmin({this.listRek, this.callback});

  @override
  _Middle3ContainerAdminState createState() => _Middle3ContainerAdminState();
}

class _Middle3ContainerAdminState extends State<Middle3ContainerAdmin> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("cek isi");
    print(widget.listRek);
  }
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height / 4.5,
      child: Row(
        children: [
          Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 2,
                  scrollDirection: Axis.horizontal,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int i) {
                    return Row(
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailProdukAdmin(
                                                    idBarang: widget.listRek[i].idBarang,
                                                    images: widget.listRek[i].gambar,
                                                    namaBarang:
                                                        widget.listRek[i].namaBarang,
                                                    harga: widget.listRek[i].harga.toString(),
                                                    deskripsi:
                                                        widget.listRek[i].deskripsi,
                                                    qty: widget.listRek[i].qty,
                                                    diskon: widget.listRek[i].promo.toString(),
                                                    hargaPromo:
                                                        widget.listRek[i].hargaPromo.toString(),
                                                  )));
                          },
                          child: Card(
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: contentColor,
                                  borderRadius: BorderRadius.all(Radius.circular(30)),
                                  ),
                              // padding: EdgeInsets.all(10),
                              height: size.height / 4,
                              width: size.width / 2.4,
                              child: Stack(
                                children: [
                                  Column(
                                    children: [
                                      Container(
                                        width: size.width,
                                        height: (size.height / 4) / 2,
                                        child: new Image.network(
                                          domain+'gambar/' +
                                              widget.listRek[i].gambar.toString(),
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                      SizedBox(height: 5,),
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        alignment: Alignment.centerLeft,
                                        child: Text(widget.listRek[i].namaBarang.toString(), style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left:10),
                                        alignment: Alignment.centerLeft,
                                        child: Text((widget.listRek[i].promo == 0)?"IDR "+widget.listRek[i].harga.toString():"IDR "+widget.listRek[i].hargaPromo.toString(), style: TextStyle(color: colorGrey, fontWeight: FontWeight.w600),),
                                      )
                                    ],
                                  ),
                                  (widget.listRek[i].promo != 0)?Positioned(
                                    top: -20,
                                    right: -45,
                                    child: Container(
                                        alignment: Alignment.center,
                                        width: 100,
                                        height: 20,
                                        transform: Matrix4.rotationZ(0.8),
                                        color: Colors.red,
                                        child: Text(
                                          widget.listRek[i].promo.toString()+"%",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )),
                                  ):Container(),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        )
                      ],
                    );
                  }))
        ],
      ),
    );
  }
}
