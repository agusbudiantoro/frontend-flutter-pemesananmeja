import 'dart:ui';

import 'package:flutter/material.dart';

class SphareHome extends StatefulWidget {
  final double width;
  final double height;
  final Widget child;

  SphareHome({this.height, this.width, this.child});
  @override
  _SphareHomeState createState() => _SphareHomeState();
}

class _SphareHomeState extends State<SphareHome> {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    var size = MediaQuery.of(context).size;
    return Container(
      // margin: EdgeInsets.only(left: 10,top: 10/2,bottom: 10*2.5),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(80.0),
            bottomRight: Radius.circular(80.0)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            child: Column(
              children: [
                Container(
                    alignment: Alignment.bottomCenter,
                    width: size.width,
                    height: size.height / 8,
                    color: Colors.transparent,
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Image.asset("assets/images/leaf.png", width: 50),
                        Container(
                          child: Row(
                            children: [
                              Text(
                                "Q",
                                style: TextStyle(
                                    fontSize: 45,
                                    fontFamily: 'RobotoCondensed',
                                    color: Colors.white),
                              ),
                              Text("ia",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                    fontFamily: 'RobotoCondensed',
                                  ))
                            ],
                          ),
                        ),

                      ],
                    )),
              ],
            ),
            height: widget.height,
            width: widget.width,
            decoration: BoxDecoration(
                gradient: RadialGradient(radius: 50, colors: [
              Colors.white.withOpacity(0.20),
              Colors.white.withOpacity(0.1)
            ])),
          ),
        ),
      ),
    );
  }
}
