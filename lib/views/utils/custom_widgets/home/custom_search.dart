import 'dart:ui';

import 'package:flutter/material.dart';
// import 'package:flutter_jamu_tujuh/views/pages/feeds.dart';

class SearchHome extends StatefulWidget {
  final double width;
  final double height;

  SearchHome({this.height, this.width});
  @override
  _SearchHomeState createState() => _SearchHomeState();
}

class _SearchHomeState extends State<SearchHome> {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    var size = MediaQuery.of(context).size;
    return ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(80)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 5),
            alignment: Alignment.bottomCenter,
            height: widget.height,
            width: widget.width,
            decoration: BoxDecoration(
                gradient: RadialGradient(radius: 50, colors: [
                  Colors.white.withOpacity(0.20),
                  Colors.white.withOpacity(0.1)
                ]),
                borderRadius: BorderRadius.all(Radius.circular(80)),
                // color: Colors.white,
                shape: BoxShape.rectangle,
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 25),
                      blurRadius: 30,
                      color: Colors.white.withOpacity(0.2))
                ]),
            child: TextField(
              onTap: () {
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => FeedPage(
                //               jenis: 3,
                //             )));
              },
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  hintText: "Cari",
                  hintStyle: TextStyle(color: Colors.white.withOpacity(0.5)),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  suffixIcon: Icon(
                    Icons.search,
                    color: Colors.white,
                  )),
            ),
          ),
        ));
  }
}
