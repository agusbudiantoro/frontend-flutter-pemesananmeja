import 'dart:ui';

import 'package:flutter/material.dart';

class ContainerHomeChild extends StatefulWidget {
  final double height;
  final double width;
  final Widget child;
  final double borderRadius;

  ContainerHomeChild({this.height, this.width, this.child, this.borderRadius = 20.0});
  @override
  _ContainerHomeChildState createState() => _ContainerHomeChildState();
}

class _ContainerHomeChildState extends State<ContainerHomeChild> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 10,top: 10/2,bottom: 10*2.5),
          child: ClipRRect(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(widget.borderRadius), bottomLeft:Radius.circular(widget.borderRadius), topRight: Radius.circular(widget.borderRadius), bottomRight: Radius.circular(widget.borderRadius)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Container(
            child: widget.child,
            height: widget.height,
            width: widget.width,
            decoration: BoxDecoration(
              gradient: RadialGradient(
                radius: 50,
                colors: [
                  Colors.white.withOpacity(0.20),
                  Colors.white.withOpacity(0.1)
                ]
              )
            ),
          ),
        ),
      ),
    );
  }
}