import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/historyPembelian/historyPembelian.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/keranjang/main.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/pagePembayaran.dart/main.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/daftarBelanja.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/keranjang/formKeranjang.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/barangKeranjang_bloc/barangkeranjang_bloc.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/keranjang/detailKeranjang/daftarBelanja.dart';

class ModalBuy extends StatefulWidget {
  final BarangModel data;
  ModalBuy({this.data});
  @override
  _ModalBuyState createState() => _ModalBuyState();
}

class _ModalBuyState extends State<ModalBuy> {
  bool statusPass = false;
  bool openDesk = false;
  int total_hargaBarang;
  int stokBarang;
  int qty=1;
  BarangModel myData;
  BarangkeranjangBloc blocKeranjang = BarangkeranjangBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (int.parse(widget.data.diskon) != 0) {
      total_hargaBarang = int.parse(widget.data.hargaPromo) * qty;
    } else {
      total_hargaBarang = int.parse(widget.data.harga) * qty;
    }
    stokBarang = widget.data.qty;
    myData = BarangModel(stok: stokBarang,image: widget.data.image,totalHarga: total_hargaBarang, idBarang: widget.data.idBarang, deskripsi: widget.data.deskripsi, harga: widget.data.harga.toString(), hargaPromo: widget.data.hargaPromo.toString(),namaBarang: widget.data.namaBarang, qty: qty, diskon: widget.data.diskon.toString(),kategori: widget.data.kategori.toString());
  }

  void _clickCallBackPlus() {
    setState(() {
      if (qty < stokBarang && qty >= 1) {
        qty += 1;
        if(int.parse(widget.data.diskon) != 0){
          total_hargaBarang = int.parse(widget.data.hargaPromo) * qty;
        } else {
          total_hargaBarang = int.parse(widget.data.harga) * qty;
        }
      }
    });
  }

  void _clickCallBackMin() {
    setState(() {
      if (qty <= stokBarang && qty > 1) {
        qty -= 1;
        if(int.parse(widget.data.diskon) != 0){
          total_hargaBarang = int.parse(widget.data.hargaPromo) * qty;
        } else {
          total_hargaBarang = int.parse(widget.data.harga) * qty;
        }
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height/3,
      // decoration:BoxDecoration(
      //   // color: Colors.red,
      //   borderRadius: BorderRadius.only(topLeft: Radius.circular(88), topRight: Radius.circular(88))
      // ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          WidgetDaftarBelanja(width: size.width,height: size.height,dataBarang: myData,clickCallbackPlus: ()=>_clickCallBackPlus(),clickCallbackMin: ()=>_clickCallBackMin(),),
          Spacer(),
          Row(
            children: [
              SizedBox(
                      width: size.width / 2,
                      height: 74,
                      child: ElevatedButton(
                        onPressed: () {
                          BarangModel myData = BarangModel(stok: stokBarang,image: widget.data.image,totalHarga: total_hargaBarang, idBarang: widget.data.idBarang, deskripsi: widget.data.deskripsi, harga: widget.data.harga.toString(), hargaPromo: widget.data.hargaPromo.toString(),namaBarang: widget.data.namaBarang, qty: qty, diskon: widget.data.diskon.toString(),kategori: widget.data.kategori.toString());
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>Pagebayar(dataBarang: myData,)));
                        },
                        child: Text("Beli",
                            style: TextStyle(
                                color:colorGrey)),
                        style: ButtonStyle(
                            elevation: MaterialStateProperty.all<double>(0),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                contentColor),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(20))))),
                      ),
                    ),
                  
              Container(
                width: size.width / 2,
                height: 74,
                child: BlocListener<BarangkeranjangBloc, BarangkeranjangState>(
                  bloc:blocKeranjang,
                  listener: (context, state) {
                    // TODO: implement listener
                    if(state is StatePostBarangKeranjangSukses){
                          Navigator.pop(context);
                          Navigator.push((context), MaterialPageRoute(builder:(context)=>BackgroundKeranjang()));
                      }
                  },
                  child: Container(
                    child: BlocBuilder<BarangkeranjangBloc, BarangkeranjangState>(
                      bloc: blocKeranjang,
                      builder: (context, state) {
                        if(state is StatePostBarangKeranjangSukses){
                          return buttonSimpanKeranjang();
                        }
                        if(state is StatePostBarangKeranjangWaiting){
                          return Container(child:Center(child: CircularProgressIndicator(),));
                        }
                        if(state is StatePostBarangKeranjangFailed){
                          return buttonSimpanKeranjang();
                        }
                        return buttonSimpanKeranjang();
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ElevatedButton buttonSimpanKeranjang() {
    return ElevatedButton(
      onPressed: () {
        FormKeranjangModel data = FormKeranjangModel(
            totalHarga: (int.parse(widget.data.diskon) != 0)
                ? (int.parse(widget.data.hargaPromo) * qty)
                : (int.parse(widget.data.harga) * qty),
            idBarang: widget.data.idBarang,
            qty: qty);
        blocKeranjang..add(EventPostBarangKeranjang(data: data));
      },
      child: Text(
        "Tambah ke Keranjang",
        style: TextStyle(color: colorGrey),
      ),
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(
              contentColor),
          elevation: MaterialStateProperty.all<double>(0),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.only(topLeft: Radius.circular(20))))),
    );
  }
}
