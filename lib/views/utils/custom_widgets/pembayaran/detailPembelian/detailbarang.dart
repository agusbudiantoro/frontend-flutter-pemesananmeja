import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';

class WidgetDaftarBelanjaPembelian extends StatefulWidget {
  final double width;
  final double height;
  final BarangModel dataBarang;

  WidgetDaftarBelanjaPembelian({this.height, this.width, this.dataBarang});
  @override
  _WidgetDaftarBelanjaPembelianState createState() => _WidgetDaftarBelanjaPembelianState();
}

class _WidgetDaftarBelanjaPembelianState extends State<WidgetDaftarBelanjaPembelian> {
  int qty;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    qty = widget.dataBarang.qty;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(15),
      height: widget.height / 3.5,
      width: widget.width,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              "Daftar Belanja",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            height: widget.height / 9,
            width: widget.width,
            child: Row(
              children: [
                Container(
                    width: widget.width / 5,
                    height: widget.height / 5,
                    child: Column(
                      children: [
                        Container(
                          height: widget.height / 12,
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: new Image.network(
                              domain+'gambar/'+
                              widget.dataBarang.image.toString(),
                              // scale: 2,
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                      ],
                    )),
                Container(
                  padding: EdgeInsets.all(10),
                  width: (widget.width - 38) - (widget.width / 5),
                  height: widget.height,
                  // color: Colors.white,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Flexible(
                            child: Text(
                              widget.dataBarang.namaBarang,
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Row(
                        children: [
                          Flexible(
                            child: Text(
                              widget.dataBarang.qty.toString()+" x "+"Rp "+widget.dataBarang.harga.toString(),
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Text(
                              "Rp "+(int.parse(widget.dataBarang.harga)*widget.dataBarang.qty).toString(),
                              maxLines: 1,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 5,),
          // Column(
          //   children: [
          //     Container(
          //     child: new Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry."),
          //     decoration: new BoxDecoration(
          //       color: Colors.white,
                
          //     ),
          //   ),
          //   ],
          // )
        ],
      ),
    );
  }
}
