import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';

class WidgetHistoryPengiriman extends StatefulWidget {
  final double width;
  final double height;
  final int statusPengiriman;
  WidgetHistoryPengiriman({this.width, this.height, this.statusPengiriman});
  @override
  _WidgetHistoryPengirimanState createState() =>
      _WidgetHistoryPengirimanState();
}

class _WidgetHistoryPengirimanState extends State<WidgetHistoryPengiriman> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(10),
      // height: widget.width/3.5,
      width: widget.width,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              "Status Pengiriman",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.grey,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 5, bottom: 10, right: 5, left: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        child: Row(
                      children: [
                        Container(
                          child: Text((widget.statusPengiriman == 0)?"Menunggu Pembayaran":(widget.statusPengiriman == 1)?"Barang Sedang Disiapkan":(widget.statusPengiriman == 2)?"Barang Sedang Dikirm":(widget.statusPengiriman == 3)?"Barang Sudah Sampai":""),
                        )
                      ],
                    )),
                    // SizedBox(width: 5,),
                    // Container(
                    //     child: TextButton(
                    //         onPressed: () {},
                    //         child: Text(
                    //           "Lihat Detail",
                    //           style: TextStyle(
                    //               color: Colors.black,
                    //               fontWeight: FontWeight.bold),
                    //         ))),
                  ],
                ),
              ),
              Container(
                padding:
                    EdgeInsets.only(top: 15, bottom: 10, right: 5, left: 5),
                child: Container(
                  height: 1,
                  width: widget.width,
                  color: Colors.grey.withOpacity(0.3),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
