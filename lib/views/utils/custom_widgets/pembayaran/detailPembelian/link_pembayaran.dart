import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class WidgetLink extends StatefulWidget {
  final double width;
  final double height;
  final String link;
  WidgetLink({this.height, this.width, this.link});
  @override
  _WidgetLinkState createState() => _WidgetLinkState();
}

class _WidgetLinkState extends State<WidgetLink> {

  void _launchURL(link) async {
    if (!await launch(link)) throw 'Could not launch $link';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(15),
              height: widget.height/6,
              width: widget.width,
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left:5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Link Pembayaran",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => _launchURL(widget.link),
                      child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                      widget.link.toString(),
                                      style: TextStyle(
                                          fontFamily: 'RobotoCondensed'),
                                      textAlign: TextAlign.left),
                                ],
                              ),
                            ),
                            // SizedBox(width: 5,),
                            // Icon(Icons.arrow_forward_ios_sharp),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      )
                    ],
                  )),
                ],
              ),
            );
  }
}