import 'package:flutter/material.dart';

import 'childInformasiPesanan.dart/metodePembayaran.dart';
import 'childInformasiPesanan.dart/statusPesanan.dart';
import 'childInformasiPesanan.dart/totalPembayaran.dart';

class WidgetInformasiPesanan extends StatefulWidget {
  final double width;
  final double height;
  final String statusCode;
  final String status;
  final String waktuPembayaran;
  final String totalHarga;
  final String namaPembayaran;
  final String logoPembaayaran;

  WidgetInformasiPesanan({this.width, this.height, this.status, this.statusCode, this.waktuPembayaran, this.logoPembaayaran, this.namaPembayaran, this.totalHarga});
  @override
  _WidgetInformasiPesananState createState() => _WidgetInformasiPesananState();
}

class _WidgetInformasiPesananState extends State<WidgetInformasiPesanan> {
  @override
  Widget build(BuildContext context) {
    return Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(15),
              // height: widget.width/3.1,
              width: widget.width,
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left:5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Informasi Pesanan",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      ChildWidgetStatusPesanan(statusCode: widget.statusCode.toString(),status: widget.status.toString(),waktuPembayaran: widget.waktuPembayaran.toString(),width: widget.width,),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      ChildWidgetTotalPesanan(width: widget.width,hargaTotal: widget.totalHarga.toString(),jasaPengiriman: "10000",),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      ),
                      MetodePembayaran(width: widget.width,height: widget.height,logoPembayaran: widget.logoPembaayaran,namaPembayaran: widget.namaPembayaran,),
                ],
              ),
            );
  }
}