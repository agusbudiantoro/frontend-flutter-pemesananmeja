import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';

class MetodePembayaran extends StatefulWidget {
  final double width;
  final double height;
  final String namaPembayaran;
  final String logoPembayaran;
  MetodePembayaran({this.width, this.height, this.logoPembayaran, this.namaPembayaran});
  @override
  _MetodePembayaranState createState() => _MetodePembayaranState();
}

class _MetodePembayaranState extends State<MetodePembayaran> {
  @override
  Widget build(BuildContext context) {
    return Container(
              alignment: Alignment.centerLeft,
              // padding: EdgeInsets.all(5),
              // height: widget.width/3.5,
              width: widget.width,
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left:5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Metode Pembayaran",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                      widget.namaPembayaran,
                                      style: TextStyle(
                                          fontFamily: 'RobotoCondensed'),
                                      textAlign: TextAlign.left),
                                ],
                              ),
                            ),
                            SizedBox(width: 5,),
                            Container(
                              height: 20,
                              padding: EdgeInsets.only(right:10),
                              // width: widget.width/4.5,
                              // color: Colors.red,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    height: 20,
                                    width: widget.width/7,
                                    child: new Image.network(
                                      domain+'logo/'+widget.logoPembayaran.toString(),
                                      // scale: 2,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      )
                    ],
                  )
                ],
              ),
            );
  }
}