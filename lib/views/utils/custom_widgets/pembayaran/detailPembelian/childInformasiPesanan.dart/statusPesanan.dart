import 'package:flutter/material.dart';

class ChildWidgetStatusPesanan extends StatefulWidget {
  final String status;
  final String waktuPembayaran;
  final String statusCode;
  final double width;
  ChildWidgetStatusPesanan({this.width,this.status, this.waktuPembayaran, this.statusCode});
  @override
  _ChildWidgetStatusPesananState createState() => _ChildWidgetStatusPesananState();
}

class _ChildWidgetStatusPesananState extends State<ChildWidgetStatusPesanan> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text("STATUS PESANAN", style: TextStyle(color: Colors.grey, fontSize: 10),)),
          SizedBox(height: 5,),
          Container(
            alignment:Alignment.centerLeft,
            child: (widget.statusCode == "200")?Text(widget.status.toString()+" "+"("+widget.waktuPembayaran+")"):Text(widget.status.toString())
            )
        ],
      ),
      
    );
  }
}