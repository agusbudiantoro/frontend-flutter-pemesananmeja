import 'package:flutter/material.dart';

class WidgetAlamatPem extends StatefulWidget {
  final double width;
  final double height;
  final String alamat;
  WidgetAlamatPem({this.height, this.width, this.alamat});
  @override
  _WidgetAlamatPemState createState() => _WidgetAlamatPemState();
}

class _WidgetAlamatPemState extends State<WidgetAlamatPem> {
  @override
  Widget build(BuildContext context) {
    return Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(15),
              height: widget.height/6,
              width: widget.width,
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left:5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Alamat Penerima",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  GestureDetector(
                      child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                      widget.alamat.toString(),
                                      style: TextStyle(
                                          fontFamily: 'RobotoCondensed'),
                                      textAlign: TextAlign.left),
                                ],
                              ),
                            ),
                            // SizedBox(width: 5,),
                            // Icon(Icons.arrow_forward_ios_sharp),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top:15, bottom: 10, right: 5, left: 5),
                        child: Container(
                          height: 1,
                          width: widget.width,
                          color: Colors.grey.withOpacity(0.3),
                        ),
                      )
                    ],
                  )),
                ],
              ),
            );
  }
}