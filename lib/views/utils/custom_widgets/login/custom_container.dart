import 'dart:ui';

import 'package:flutter/material.dart';

class ContainerLogin extends StatefulWidget {
  final double height;
  final double width;
  final Widget child;
  final double borderRadius;

  ContainerLogin({this.height, this.width, this.child, this.borderRadius = 20.0});
  @override
  _ContainerLoginState createState() => _ContainerLoginState();
}

class _ContainerLoginState extends State<ContainerLogin> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(widget.borderRadius)),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
              child: Container(
          child: widget.child,
          height: widget.height,
          width: widget.width,
          decoration: BoxDecoration(
            gradient: RadialGradient(
              radius: 50,
              colors: [
                Colors.white.withOpacity(0.20),
                Colors.white.withOpacity(0.1)
              ]
            )
          ),
        ),
      ),
    );
  }
}