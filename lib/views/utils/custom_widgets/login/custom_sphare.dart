import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

class SphareLogin extends StatefulWidget {
  final double width;
  final double height;

  SphareLogin({this.height, this.width});
  @override
  _SphareLoginState createState() => _SphareLoginState();
}

class _SphareLoginState extends State<SphareLogin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      decoration: BoxDecoration(
        gradient:LinearGradient(
          colors: [
            circlePurpleDark,
            circlePurpleLight
          ]
        ),
        shape: BoxShape.circle
      ),
    );
  }
}