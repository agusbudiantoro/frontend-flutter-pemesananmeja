import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef HapusData = void Function(int);
class DialogDelete extends StatefulWidget {
  final HapusData callback;
  final int id;
  DialogDelete({ this.callback, this.id});

  @override
  _DialogDeleteState createState() => _DialogDeleteState();
}

class _DialogDeleteState extends State<DialogDelete> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CupertinoAlertDialog(
                title: const Text('Hapus'),
                content: const Text('Apakah anda yakin ingin menghapus data ini ?'),
                actions: <CupertinoDialogAction>[
                  CupertinoDialogAction(
                    child: const Text('No'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  CupertinoDialogAction(
                    child: const Text('Yes'),
                    isDestructiveAction: true,
                    onPressed: () {
                      widget.callback(widget.id);
                      Navigator.pop(context);
                      // Do something destructive.
                    },
                  )
                ],
              ),
    );
  }
}