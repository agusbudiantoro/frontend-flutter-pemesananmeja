import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

// ignore: must_be_immutable
class TextFieldRegister extends StatefulWidget {
  String hintText;
  TextEditingController isiField = TextEditingController();
  IconData prefixIcon;
  IconData suffixIcon;
  bool isObsercure;
  VoidCallback clickCallback;
  VoidCallback onChange;
  VoidCallback onTap;
  bool readOnly;
  bool typeInput;
  bool typePass;

  TextFieldRegister(
      {this.hintText,
      this.isObsercure = false,
      this.prefixIcon,
      this.suffixIcon, this.clickCallback, this.isiField, this.typeInput, this.typePass, this.onChange, this.readOnly, this.onTap});
  @override
  _TextFieldRegisterState createState() => _TextFieldRegisterState();
}

class _TextFieldRegisterState extends State<TextFieldRegister> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      // decoration: BoxDecoration(
      //     border: Border.all(width: 2.0, color: Colors.white),
      //     borderRadius: BorderRadius.all(Radius.circular(30.0))),
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: [
          Icon(
            widget.prefixIcon,
            color: colorGrey,
          ),
          SizedBox(width: 5.0),
          Expanded(
              child: TextField(
                readOnly: (widget.readOnly != null)?false:widget.readOnly,
                onTap: (){
                  print("cek");
                  (widget.onTap != null)?widget.onTap():print("tes");
                },
                onChanged: (val){
                  if(widget.onChange != null){
                    widget.onChange();
                  }
                },
                cursorColor: Colors.black,
                minLines: 1,
                maxLines: (widget.typePass == true)?1:5, // jika type password maka maxlines == 1, jikan bukan password maka maxlines bisa 5
                keyboardType: (widget.typeInput == true)?TextInputType.number:TextInputType.multiline,
                controller: widget.isiField,
            obscureText: widget.isObsercure,
            style: TextStyle(color: Colors.black, fontSize: 20.0),
            decoration: InputDecoration(
              focusColor: colorGrey,
                hintText: widget.hintText,
                hintStyle: TextStyle(
                    color: colorGrey.withOpacity(0.8), fontSize: 18.0),
                border: InputBorder.none,
                focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: colorGrey),
              ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: colorGrey),
                ),
                ),
          )),
          SizedBox(width: 5),
          GestureDetector(
              onTap: () {
                widget.clickCallback();
              },
              child: widget.suffixIcon != null
                  ? Icon(
                      widget.suffixIcon,
                      color: Colors.white,
                    )
                  : Container())
        ],
      ),
    );
  }
}
