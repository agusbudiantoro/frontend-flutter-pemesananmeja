import 'package:flutter/material.dart';

class WidgetRincianHarga extends StatefulWidget {
  final double width;
  final double height;
  int totalHargaBarang;
  WidgetRincianHarga({this.height, this.width, this.totalHargaBarang});
  @override
  _WidgetRincianHargaState createState() => _WidgetRincianHargaState();
}

class _WidgetRincianHargaState extends State<WidgetRincianHarga> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(15),
      height: widget.height / 6,
      width: widget.width,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              "Rincian Harga",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10),
          Container(
              padding: EdgeInsets.all(5),
              alignment: Alignment.centerLeft,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text("Tota Harga Barang"), Text("Rp "+widget.totalHargaBarang.toString())],
                  ),
                  SizedBox(height:5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text("Ongkos Kirim"), Text("Rp 10.000")],
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
