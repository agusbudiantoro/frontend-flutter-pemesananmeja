import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/alamatPengiriman/alamat.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/pagePembayaran.dart/pageAlamat/alamat.dart';
// import 'package:flutter_jamu_tujuh/views/widgets/pembayaran/getLocation/alamatPengiriman.dart';
typedef AlamatPengirimanDef = void Function(String);
class WidgetAlamat extends StatefulWidget {
  final double width;
  final double height;
  final AlamatPengirimanDef callback;
  WidgetAlamat({this.width, this.height, this.callback});
  @override
  _WidgetAlamatState createState() => _WidgetAlamatState();
}

class _WidgetAlamatState extends State<WidgetAlamat> {
  alamatPengirimanModel dataAlamat;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        dataAlamat = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => GetLokasiForAlamat()));
                setState(() {
                  dataAlamat = dataAlamat;
                });
                widget.callback((dataAlamat != null)?dataAlamat.fromGps+", "+dataAlamat.tambahanAlamat:"");
      },
      child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.all(15),
        height: widget.height / 5,
        width: widget.width,
        color: Colors.white,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 5),
              alignment: Alignment.centerLeft,
              child: Text(
                "Alamat Pengiriman",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Column(
              children: [
                Container(
                  padding:
                      EdgeInsets.only(top: 15, bottom: 10, right: 5, left: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(
                                (dataAlamat != null)
                                    ? ((dataAlamat.fromGps != null)
                                                ? dataAlamat.fromGps.toString()+" "
                                                : '')
                                            .toString() +
                                        ((dataAlamat.tambahanAlamat != null)
                                                ? dataAlamat.tambahanAlamat
                                                    .toString()
                                                : "")
                                            .toString()
                                    : "",
                                style: TextStyle(fontFamily: 'RobotoCondensed'),
                                textAlign: TextAlign.left),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(Icons.arrow_forward_ios_sharp),
                    ],
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.only(top: 15, bottom: 10, right: 5, left: 5),
                  child: Container(
                    height: 1,
                    width: widget.width,
                    color: Colors.grey.withOpacity(0.3),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
