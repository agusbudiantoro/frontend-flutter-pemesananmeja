import 'package:flutter/material.dart';

const Color purpleBack = Color(0xffe8c372);
const Color blueBack = Color(0xff79d1d9);
const Color circlePurpleLight = Color(0xff43cea2);
const Color circlePurpleDark = Color(0xffa67053);
const Color background1 = Colors.white;
const Color colorGrey = Color(0xFF383838);
const Color captionColor = Color(0xFFefebe5);
const Color contentColor = Color(0xFFd2c9b5);