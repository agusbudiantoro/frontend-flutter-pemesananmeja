import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_jamu_tujuh/models/model_paket_waktu/model_paket_waktu.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/paketWaktuApi/paketWaktuApi.dart';
import 'package:meta/meta.dart';

part 'paketwaktu_event.dart';
part 'paketwaktu_state.dart';

class PaketwaktuBloc extends Bloc<PaketwaktuEvent, PaketwaktuState> {
  PaketwaktuBloc() : super(PaketwaktuInitial());

  @override
  Stream<PaketwaktuState> mapEventToState(
    PaketwaktuEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventGetPaketWaktu){
      yield* _getAllPaketWaktu();
    }
  }
}

Stream <PaketwaktuState> _getAllPaketWaktu()async*{
  yield StateGetPaketWaktuWaiting();
  try {
    dynamic data = await PaketWaktuApi.getPaketWaktu();
    var convertData = jsonDecode(data);
    ModelPaketWaktu dataAwal = ModelPaketWaktu.fromJson(convertData);
    List<ValuePaketWaktu> dataAkhir = dataAwal.value;

    yield StateGetPaketWaktuSukses(data: dataAkhir);
  } catch (e) {
    yield StateGetPaketWaktuFailed(errorMessage: e.message.toString());
  }
}