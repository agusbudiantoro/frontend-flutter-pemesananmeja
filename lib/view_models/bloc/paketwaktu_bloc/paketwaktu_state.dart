part of 'paketwaktu_bloc.dart';

@immutable
abstract class PaketwaktuState {}

class PaketwaktuInitial extends PaketwaktuState {}

class StateGetPaketWaktuSukses extends PaketwaktuState{
  List<ValuePaketWaktu> data;
  StateGetPaketWaktuSukses({this.data});
}

class StateGetPaketWaktuFailed extends PaketwaktuState {
  final String errorMessage;
  StateGetPaketWaktuFailed({this.errorMessage});
}

class StateGetPaketWaktuWaiting extends PaketwaktuState{
  
}