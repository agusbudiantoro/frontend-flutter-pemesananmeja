import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_jamu_tujuh/models/keranjang/barang.dart';
import 'package:flutter_jamu_tujuh/models/keranjang/formKeranjang.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/barangKeranjang/apiKeranjang.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'barangkeranjang_event.dart';
part 'barangkeranjang_state.dart';

class BarangkeranjangBloc extends Bloc<BarangkeranjangEvent, BarangkeranjangState> {
  BarangkeranjangBloc() : super(BarangkeranjangInitial());

  @override
  Stream<BarangkeranjangState> mapEventToState(
    BarangkeranjangEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventPostBarangKeranjang){
      yield* _postBarangKeranjang(event.data);
    }
    if(event is EventGetBarangKeranjangByIdKonsumen){
      yield* _getBarangKeranjangByIdKonsumen();
    }
    if(event is EventDelBarangKeranjang){
      yield* _delBarangKeranjang(event.id);
    }
  }
}

Stream<BarangkeranjangState> _delBarangKeranjang(id)async*{
  yield StateDelBarangKeranjangWaiting();
  try {
    print(id);
    print("cek delete");
    bool res = await KeranjangApi.delBarangKeranjang(id);
    if(res == true){
      yield StateDelBarangKeranjangSukses();
    } else{
      yield StateDelBarangKeranjangFailed();
    }
  } catch (e) {
    yield StateDelBarangKeranjangFailed(errorMessage: e.toString());
  }
}


Stream<BarangkeranjangState> _postBarangKeranjang(data)async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield StatePostBarangKeranjangWaiting();
  try {
    int id = await preferences.getInt('myId');
    if(id != null){
      bool value = await KeranjangApi.postBarangKeranjang(id, data);
      if(value == true){
        yield StatePostBarangKeranjangSukses();
      } else {
        yield StatePostBarangKeranjangFailed();
      }
      yield StatePostBarangKeranjangSukses();
    } else {
      yield StatePostBarangKeranjangFailed();
    }
  } catch (e) {
    yield StatePostBarangKeranjangFailed();
  }
}

Stream<BarangkeranjangState> _getBarangKeranjangByIdKonsumen()async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield StateGetBarangKeranjangByIdKonsumenWaiting();
  try {
    int id = await preferences.getInt('myId');
    if(id != null){
      dynamic value = await KeranjangApi.getBarangKeranjangByIdKonsumen(id);
      var isi = jsonDecode(value);
      ListValueBarangKeranjang hasilConvertValue = ListValueBarangKeranjang.fromJson(isi);
      List<ValuesListBarangKeranjang> data = hasilConvertValue.value;
      yield StateGetBarangKeranjangByIdKonsumenSukses(data: data);
    } else {
      yield StateGetBarangKeranjangByIdKonsumenFailed();
    }
  } catch (e) {
    yield StateGetBarangKeranjangByIdKonsumenFailed();
  }
}