part of 'pesanan_bloc.dart';

@immutable
abstract class PesananEvent {}

class EventPostPesanan extends PesananEvent {
  ModelRiwayatPesanan data;
  EventPostPesanan({this.data});
}

class EventGetAllPemesanan extends PesananEvent {
  
}

class EventDeletePesanan extends PesananEvent {
  final ValueGetPemesanan data;
  EventDeletePesanan({this.data});
}

class EventPutStatusPesanan extends PesananEvent {
  final ValueGetPemesanan data;
  EventPutStatusPesanan({this.data});
}