import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_jamu_tujuh/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:flutter_jamu_tujuh/models/modalLink/link.dart';
import 'package:flutter_jamu_tujuh/models/pesanan/modelRiwayatPesanan.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/kirimEmail/kirimEmail.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/pesanan/pesananApi.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'pesanan_event.dart';
part 'pesanan_state.dart';

class PesananBloc extends Bloc<PesananEvent, PesananState> {
  PesananBloc() : super(PesananInitial());

  @override
  Stream<PesananState> mapEventToState(
    PesananEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventPostPesanan){
      yield* _postPesanan(event.data);
    }
    if(event is EventGetAllPemesanan){
      yield* _getAllPesanan();
    }
    if(event is EventDeletePesanan){
      yield* _delPesanan(event.data);
    }
    if(event is EventPutStatusPesanan){
      yield* _putStatusPesanan(event.data);
    }
  }
}

Stream<PesananState> _putStatusPesanan(data)async*{
  yield StatePutStatusPesananWaiting();
  try {
    bool value = await ApiPesanan.putStatusPesanan(data);
    if(value == true){
      yield StatePutStatusPesananSukses();
    } else {
      yield StatePutStatusFailed();
    }
  } catch (e) {
    yield StatePutStatusFailed(errorMessage: e.toString());
  }
}

Stream<PesananState> _delPesanan(data)async*{
  yield StateDeletePesananWaiting();
  try {
    bool value = await ApiPesanan.deletePesanan(data);
    if(value == true){
      yield StateDeletePesananSukses();
    } else {
      yield StateDeleteFailed();
    }
  } catch (e) {
    yield StateDeleteFailed(errorMessage: e.toString());
  }
}

Stream<PesananState> _getAllPesanan()async*{
  yield StateGetAllPesananWaiting();
  try {
    dynamic value = await ApiPesanan.getAllPesanan();
    var hasil = jsonDecode(value);
    ModelGetPemesanan hasilConvert = ModelGetPemesanan.fromJson(hasil);
    List<ValueGetPemesanan> hasilGetHistoryPembayaran = hasilConvert.value;
    yield StateGetAllPesananSukses(data: hasilGetHistoryPembayaran);
  } catch (e) {
    yield StateGetAllPesananFailed(errorMessage: e.toString());
  }
}

Stream<PesananState> _postPesanan(ModelRiwayatPesanan data)async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
  yield StatePostPesananWaiting();
  try {
    int id = await preferences.getInt('myId');
    String email = await preferences.getString("myEmail");
    // dynamic value = await KirimEmailNetwork.postEmail();
    dynamic value = await ApiPesanan.postPesanan(id, data);
    // var isi = jsonDecode(value);
    // ModalLink link = ModalLink.fromJson(isi);
    // dynamic valueEmail = await KirimEmailNetwork.postEmail(link.link, email);
    yield StatePostPesananSukses();
  } catch (e) {
    yield StatePostPesananFailed(errorMessage: e.message.toString());
  }
}
