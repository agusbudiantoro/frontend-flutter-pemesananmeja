part of 'bloc_login_bloc.dart';

@immutable
abstract class BlocLoginEvent {}

class BlocEventLogin extends BlocLoginEvent{
  final MyDataFormLogin myData;
  BlocEventLogin({this.myData});
}

class BlocCekToken extends BlocLoginEvent {
  final String token;
  BlocCekToken({this.token});
}

class BlocRegister extends BlocLoginEvent {
  final MyDataFormLogin data;

  BlocRegister({this.data});
}

class BlocGantiPass extends BlocLoginEvent {
  final MyDataFormLogin data;

  BlocGantiPass({this.data});
}