import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_jamu_tujuh/models/jadwalbookmeja/main.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/pesanan_bloc/pesanan_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/pesanLapanganApi/pesanLApanganApi.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'pesanlapangan_event.dart';
part 'pesanlapangan_state.dart';

class PesanlapanganBloc extends Bloc<PesanlapanganEvent, PesanlapanganState> {
  PesanlapanganBloc() : super(PesanlapanganInitial());

  @override
  Stream<PesanlapanganState> mapEventToState(
    PesanlapanganEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventPostPEsanLapangan){
      yield* _postPesanLapangan(event.data);
    }
    if(event is EventDeletePesanan){
      yield* _delPesanan(event.id);
    }
    if(event is EventGetPesananById){
      yield* _getPesananByTim();
    }
    if(event is EventAllGetPesanan){
      yield* _getPesananAll();
    }
  }
}

Stream <PesanlapanganState> _getPesananAll()async*{
  print("sini masuk");
  yield StateGetPesanLapanganAllWaiting();
  try {
    dynamic resData = await PesanLapanganApi.getAllJadwalPesanan();
    var isi = jsonDecode(resData) as List;
    List<ModelListJadwal> convertData = isi.map((e) => ModelListJadwal.fromJson(e)).toList();

    yield StateGetPesanLapanganAllSukses(data: convertData);
  } catch (e) {
    yield StateGetPesanLapanganAllFailed(errorMessage: e.message.toString());
  }
}

Stream <PesanlapanganState> _getPesananByTim()async*{
  SharedPreferences preferences = await SharedPreferences.getInstance();
    int id = await preferences.getInt("myId");
  print("sini masuk");
  yield StateGetPesanLapanganByIdTimWaiting();
  try {
    dynamic resData = await PesanLapanganApi.getJadwalPesananByIdTim(id);
    var isi = jsonDecode(resData) as List;
    List<ModelListJadwal> convertData = isi.map((e) => ModelListJadwal.fromJson(e)).toList();

    yield StateGetPesanLapanganByIdTimSukses(data: convertData);
  } catch (e) {
    yield StateGetPesanLapanganByIdTimFailed(errorMessage: e.message.toString());
  }
}

Stream <PesanlapanganState> _postPesanLapangan(ModelListJadwal data)async*{
  yield StatePostPesanLapanganWaiting();
  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String email = await preferences.getString("myEmail");
    print(data.idMeja);
    dynamic resData = await PesanLapanganApi.postPesanan(data);
    yield StatePostPesanLapanganSukses();
  } catch (e) {
    yield StatePostPesanLapanganFailed(errorMessage: e.message.toString());
  }
}

Stream <PesanlapanganState> _delPesanan(int id)async*{
  print("sini masuk");
  yield StateDelPesanLapanganWaiting();
  try {
    dynamic resData = await PesanLapanganApi.deletePesanan(id);
    yield StateDelPesanLapanganSukses();
  } catch (e) {
    yield StateDelPesanLapanganFailed(errorMessage: e.message.toString());
  }
}