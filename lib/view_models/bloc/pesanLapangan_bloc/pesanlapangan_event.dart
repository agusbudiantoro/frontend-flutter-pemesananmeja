part of 'pesanlapangan_bloc.dart';

@immutable
abstract class PesanlapanganEvent {}

class EventPostPEsanLapangan extends PesanlapanganEvent {
  ModelListJadwal data;
  EventPostPEsanLapangan({this.data});
}

class EventDeletePesanan extends PesanlapanganEvent{
  final int id;
  EventDeletePesanan({this.id});
}

class EventGetPesananById extends PesanlapanganEvent {
  
}

class EventAllGetPesanan extends PesanlapanganEvent {
  
}