part of 'pesanlapangan_bloc.dart';

@immutable
abstract class PesanlapanganState {}

class PesanlapanganInitial extends PesanlapanganState {}

class StatePostPesanLapanganSukses extends PesanlapanganState{
  
}

class StatePostPesanLapanganFailed extends PesanlapanganState{
  final String errorMessage;
  StatePostPesanLapanganFailed({this.errorMessage});
}

class StatePostPesanLapanganWaiting extends PesanlapanganState {
  
}

class StateDelPesanLapanganSukses extends PesanlapanganState{
  
}

class StateDelPesanLapanganFailed extends PesanlapanganState{
  final String errorMessage;
  StateDelPesanLapanganFailed({this.errorMessage});
}

class StateDelPesanLapanganWaiting extends PesanlapanganState {
  
}

class StateGetPesanLapanganByIdTimSukses extends PesanlapanganState{
 final List<ModelListJadwal> data;
 StateGetPesanLapanganByIdTimSukses({this.data}); 
}

class StateGetPesanLapanganByIdTimFailed extends PesanlapanganState{
  final String errorMessage;
  StateGetPesanLapanganByIdTimFailed({this.errorMessage});
}

class StateGetPesanLapanganByIdTimWaiting extends PesanlapanganState {
  
}

class StateGetPesanLapanganAllSukses extends PesanlapanganState{
 final List<ModelListJadwal> data;
 StateGetPesanLapanganAllSukses({this.data}); 
}

class StateGetPesanLapanganAllFailed extends PesanlapanganState{
  final String errorMessage;
  StateGetPesanLapanganAllFailed({this.errorMessage});
}

class StateGetPesanLapanganAllWaiting extends PesanlapanganState {
  
}