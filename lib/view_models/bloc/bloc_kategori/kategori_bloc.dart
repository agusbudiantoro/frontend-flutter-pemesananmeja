import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/kategori/kategoriApi.dart';

part 'kategori_event.dart';
part 'kategori_state.dart';

class KategoriBloc extends Bloc<KategoriEvent, KategoriState> {
  KategoriBloc() : super(KategoriInitial());

  @override
  Stream<KategoriState> mapEventToState(
    KategoriEvent event,
  ) async* {
    if(event is KategoriEvent){
      yield* _getKategori();
    }
  }
}

Stream<KategoriState> _getKategori()async*{
  yield KategoriStateLoading();
  try {
      dynamic value = await KategoriApi.getKategori();
      var hasil = jsonDecode(value);
      ModelKategori hasilConvert = ModelKategori.fromJson(hasil);
      List<ValuesKategori> hasilKategori = hasilConvert.values;
    yield KategoriStateSukses(myData:hasilKategori);
  } catch (e) {
    yield KategoriStateFailed(errorMessage: "gagal login");
  }
}