part of 'kategori_bloc.dart';

@immutable
abstract class KategoriState {}

class KategoriInitial extends KategoriState {}

class KategoriStateSukses extends KategoriState{
  final List<ValuesKategori> myData;
    KategoriStateSukses({this.myData});
}

class KategoriStateFailed extends KategoriState{
  final String errorMessage;
  KategoriStateFailed({this.errorMessage});
}

class KategoriStateLoading extends KategoriState{

}
