import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_jamu_tujuh/models/meja_tersedia/main.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/meja/main.dart';
import 'package:meta/meta.dart';

part 'bloc_meja_event.dart';
part 'bloc_meja_state.dart';

class BlocMejaBloc extends Bloc<BlocMejaEvent, BlocMejaState> {
  BlocMejaBloc() : super(BlocMejaInitial());

  @override
  Stream<BlocMejaState> mapEventToState(
    BlocMejaEvent event,
  ) async* {
    if(event is EventGetKetersediaanMeja){
      yield* _getKetersediaanMeja(event.paket_waktu, event.tgl);
    }
  }
}


Stream<BlocMejaState> _getKetersediaanMeja(int paket_waktu, DateTime tgl)async*{
  yield StateGetKetersediaanMejaLoad();
  try {
      dynamic value = await MejaApi.getKetersediaanMeja(paket_waktu, tgl);
      print("cek 1");
      var hasil = jsonDecode(value) as List;
      print("cek 2");
      List<ModelListMejaTersedia> hasilConvert = hasil.map((e) => ModelListMejaTersedia.fromJson(e)).toList();
      print("cek 3");
    yield StateGetKetersediaanMejaSukses(data:hasilConvert);
  } catch (e) {
    print(e.toString());
    yield StateGetKetersediaanMejaFailed(errorMessage: e.toString());
  }
}