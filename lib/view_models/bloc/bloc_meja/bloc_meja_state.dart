part of 'bloc_meja_bloc.dart';

@immutable
abstract class BlocMejaState {}

class BlocMejaInitial extends BlocMejaState {}

class StateGetKetersediaanMejaSukses extends BlocMejaState {
  final List<ModelListMejaTersedia> data;
  StateGetKetersediaanMejaSukses({this.data});
}

class StateGetKetersediaanMejaLoad extends BlocMejaState {
  
}

class StateGetKetersediaanMejaFailed extends BlocMejaState {
  final String errorMessage;

  StateGetKetersediaanMejaFailed({this.errorMessage});
}