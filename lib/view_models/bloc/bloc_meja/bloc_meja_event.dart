part of 'bloc_meja_bloc.dart';

@immutable
abstract class BlocMejaEvent {}

class EventGetKetersediaanMeja extends BlocMejaEvent {
  final int paket_waktu;
  final DateTime tgl;

  EventGetKetersediaanMeja({this.paket_waktu, this.tgl});
}