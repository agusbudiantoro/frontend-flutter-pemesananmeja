import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';

const urlGetPaketWaktu = domain+"auth/api/v2/getPaketWaktu";

class PaketWaktuApi {
  static Future<dynamic> getPaketWaktu()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      print("sini");
      var response = await ioClient.get(Uri.parse(urlGetPaketWaktu));
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}