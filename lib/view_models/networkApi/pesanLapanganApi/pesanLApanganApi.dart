import 'dart:io';

import 'package:flutter_jamu_tujuh/models/jadwalbookmeja/main.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';


const url = domain+"auth/api/v2/";

class PesanLapanganApi {
  static Future<dynamic> postPesanan(ModelListJadwal data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print(data);
    Map<String, String>body={
      "id_tim":data.idTim.toString(),
      "nama_tim":data.namaTim.toString(),
      "no_telp":data.noTelp.toString(),
      "tanggal_pemesanan":data.tanggalPemesanan.toString(),
      "paket_waktu":data.paketWaktu.toString(),
      "id_meja":data.idMeja.toString()
      };
    try {
      print("sini");
      var response = await ioClient.post(Uri.parse(url+'bookingMeja'), body: body);
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> deletePesanan(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print("masuk delete");
    
    try {
      print("sini");
      var response = await ioClient.delete(Uri.parse(url+"deleteJadwal/"+id.toString()));
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getJadwalPesananByIdTim(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print("masuk delete");
    
    try {
      print("sini");
      var response = await ioClient.get(Uri.parse(url+"GetAllBookingByIdTim/"+id.toString()));
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getAllJadwalPesanan()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    print("masuk delete");
    
    try {
      print("sini");
      var response = await ioClient.get(Uri.parse(url+"GetAllBooking"));
      print(response);
      print("cek res");
      // print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}