import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:flutter_jamu_tujuh/models/keranjang/formKeranjang.dart';


const urlPostBarangKeranjang = domain+"auth/api/v2/tambahBarangKeranjang";
const urlGetBarangKeranjangByIdKonsumen = domain+"auth/api/v2/tampilBarangKeranjang";
const urlDelBarangKeranjang = domain+"auth/api/v2/hapusBarangKeranjang";

class KeranjangApi {
  static Future<bool> delBarangKeranjang(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'id':id.toString()
    };
      var response = await ioClient.delete(Uri.parse(urlDelBarangKeranjang),body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else{
        return false;
      }
    } catch (e) {
      return false;
    }
  }


  static Future<dynamic> getBarangKeranjangByIdKonsumen(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'id_konsumen':id.toString()
    };
      var response = await ioClient.post(Uri.parse(urlGetBarangKeranjangByIdKonsumen), body: body);
        return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> postBarangKeranjang(int id, FormKeranjangModel data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'id_barang':data.idBarang.toString(),
      'qty':data.qty.toString(),
      'total_harga':data.totalHarga.toString(),
      'id_konsumen':id.toString()
    };
      var response = await ioClient.post(Uri.parse(urlPostBarangKeranjang), body: body);
      if(response.statusCode == 200){
        return true;
      }else{
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}