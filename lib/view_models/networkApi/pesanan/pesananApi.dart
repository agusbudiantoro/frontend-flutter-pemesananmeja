import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:flutter_jamu_tujuh/models/getPembayaran/getHistoryPembayaran.dart';
import 'package:flutter_jamu_tujuh/models/keranjang/formKeranjang.dart';
import 'package:flutter_jamu_tujuh/models/pesanan/modelRiwayatPesanan.dart';


const urlPostPesanan = domain+"auth/api/v2/postTransaksi";
const urlGetAllPesanan = domain+"auth/api/v2/getAllPesanan";
const urlDeletePesanan = domain+"auth/api/v2/hapusPesanan";
const urlPutStatusPesanan = domain+"auth/api/v2/editStatusPesanan";

class ApiPesanan {
  static Future<bool> putStatusPesanan(ValueGetPemesanan data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      "no_riwayat_pesanan":data.noRiwayatPesanan.toString(),
      "status_pengiriman":data.statusPengiriman.toString()
    };
      var response = await ioClient.put(Uri.parse(urlPutStatusPesanan), body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static Future<bool> deletePesanan(ValueGetPemesanan data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      "no_riwayat_pesanan":data.noRiwayatPesanan.toString(),
      "id_barang":data.idBarang.toString(),
      "qty":data.qty.toString()
    };
      var response = await ioClient.delete(Uri.parse(urlDeletePesanan), body: body);
      print(response.body);
      if(response.statusCode == 200){
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static Future<dynamic> getAllPesanan()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlGetAllPesanan));
      print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e);
    }
  }

  static Future<dynamic> postPesanan(int id, ModelRiwayatPesanan data)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      "id_keranjang":data.idKeranjang.toString(),
      "metode_pembayaran": data.metodePembayaran.toString(),
      "id_konsumen": id.toString(),
      "alamat": data.alamat.toString(),
      "id_barang": data.idBarang.toString(),
      "nama_barang": data.namaBarang.toString(),
      "qty": data.qty.toString(),
      "harga": data.harga.toString(),
      "total_harga": data.totalHarga.toString(),
    };
      var response = await ioClient.post(Uri.parse(urlPostPesanan), body: body);
      print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e);
    }
  }
}