import 'dart:convert';
import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' as client;
import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:path/path.dart' as path;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';
import 'package:mime/mime.dart';

const urlPostBarang = domain+"auth/api/v2/tambahBarang";
const urlGetBarang = domain+"auth/api/v2/tampilBarang";
const urlGetBarangRekom = domain+"auth/api/v2/tampilBarangRekomen";
const urlGetBarangPromo = domain+"auth/api/v2/tampilBarangPromo";
const urlPutBarang = domain+"auth/api/v2/editBarang";
const urlDeleteBarang = domain+"auth/api/v2/hapusBarang";

class BarangApi {
  static Future<dynamic> delBarang(data)async{
    BarangModel myData = data;
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'id_barang':myData.idBarang.toString()
    };
      var response = await ioClient.delete(Uri.parse(urlDeleteBarang),body: body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> putBarang(data)async{
    BarangModel myData = data;
    if(myData.path != null){
      try {
      String fileName = basename(myData.path.path);
    String base64Image = base64Encode(myData.path.readAsBytesSync());
    var stream = new client.ByteStream(DelegatingStream.typed(myData.path.openRead()));
    var length = await myData.path.length();
    var request = client.MultipartRequest("PUT", Uri.parse(urlPutBarang));
    request.fields["id_barang"] = myData.idBarang.toString();
    request.fields["nama_barang"] = myData.namaBarang.toString();
    request.fields["qty"] = myData.qty.toString();
    request.fields["harga"] = myData.harga.toString();
    request.fields["promo"] = myData.diskon.toString();
    request.fields["harga_promo"] = (myData.hargaPromo != null)?myData.hargaPromo.toString():"";
    request.fields["kategori"] = myData.kategori.toString();
    request.fields["deskripsi"] = myData.deskripsi.toString();
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept':'*/*'
    });
    var pic = await client.MultipartFile("gambar", stream, length,filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(pic);
    client.Response response = await client.Response.fromStream(await request.send());
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
    } else{
      try {
      Map<String, String>body={
      'id_barang':myData.idBarang.toString(),
      'nama_barang':myData.namaBarang.toString(),
      'qty':myData.qty.toString(),
      'harga':myData.harga.toString(),
      'promo':myData.diskon.toString(),
      'harga_promo':(myData.hargaPromo != null)?myData.hargaPromo.toString():"",
      'kategori':myData.kategori.toString(),
      'deskripsi':myData.deskripsi.toString()
    };
    Response response = await client.put(Uri.parse(urlPutBarang),body: body);
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
    }
  }

  static Future<dynamic> getBarangRekom()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlGetBarangRekom));
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getBarangPromo()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlGetBarangPromo));
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }

  static Future<dynamic> getBarang()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlGetBarang));
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
  
  static Future<dynamic> postBarang(data)async{
    BarangModel myData = data;
    try {
    String fileName = basename(myData.path.path);
    String base64Image = base64Encode(myData.path.readAsBytesSync());
    var stream = new client.ByteStream(DelegatingStream.typed(myData.path.openRead()));
    var length = await myData.path.length();
      Map<String, String>body={
      'nama_barang':myData.namaBarang.toString(),
      'qty':myData.qty.toString(),
      'harga':myData.harga.toString(),
      'promo':myData.diskon.toString(),
      'harga_promo':(myData.hargaPromo != null)?myData.hargaPromo.toString():"",
      'kategori':myData.kategori.toString()
    };
    var request = client.MultipartRequest("POST", Uri.parse(urlPostBarang));
    request.fields["nama_barang"] = myData.namaBarang.toString();
    request.fields["qty"] = myData.qty.toString();
    request.fields["harga"] = myData.harga.toString();
    request.fields["promo"] = myData.diskon.toString();
    request.fields["harga_promo"] = (myData.hargaPromo != null)?myData.hargaPromo.toString():"";
    request.fields["kategori"] = myData.kategori.toString();
    request.fields["deskripsi"] = myData.deskripsi.toString();
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept':'*/*'
    });
    var pic = await client.MultipartFile("gambar", stream, length,filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(pic);
    client.Response response = await client.Response.fromStream(await request.send());
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}