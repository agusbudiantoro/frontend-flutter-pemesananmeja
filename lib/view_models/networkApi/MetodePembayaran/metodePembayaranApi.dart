import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';


const urlGetMetodePembayaran = domain+"auth/api/v2/getMetodePembayaran";

class MetodePembayaranApi {
  static Future<dynamic> getMetodePembayaran()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlGetMetodePembayaran));
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}