import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';
import 'package:flutter_jamu_tujuh/models/keranjang/formKeranjang.dart';


const urlGetPembayaran = domain+"auth/api/v2/getPesananByIdKonsumen";

class HistoryPembayaran {
  static Future<dynamic> getHistoryPembayaranByIdKonsumen(int id)async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      Map<String, String>body={
      'id_konsumen':id.toString()
    };
      var response = await ioClient.post(Uri.parse(urlGetPembayaran), body: body);
      print(response.body);
      return response.body;
    } catch (e) {
      return throw Exception(e.message.toString());
    }
  }
}