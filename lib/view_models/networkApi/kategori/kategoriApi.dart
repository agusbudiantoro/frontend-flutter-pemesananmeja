import 'dart:io';

import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:http/http.dart' show client;
import 'package:http/io_client.dart';


const urlGetKategori = domain+"auth/api/v2/getkategori";

class KategoriApi {
  static Future<dynamic> getKategori()async{
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    try {
      var response = await ioClient.get(Uri.parse(urlGetKategori));
      return response.body;
    } catch (e) {
      return throw Exception("Data tidak ditemukan");
    }
  }
}