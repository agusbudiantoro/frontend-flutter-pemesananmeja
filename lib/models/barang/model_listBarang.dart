class ListBarangModel {
  int status;
  List<ValuesListBarang> values;

  ListBarangModel({this.status, this.values});

  ListBarangModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['values'] != null) {
      values = new List<ValuesListBarang>();
      json['values'].forEach((v) {
        values.add(new ValuesListBarang.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.values != null) {
      data['values'] = this.values.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValuesListBarang {
  int idBarang;
  String namaBarang;
  int qty;
  int harga;
  String gambar;
  int promo;
  String deskripsi;
  int hargaPromo;
  int kategori;
  int stok;

  ValuesListBarang(
      {this.idBarang,
      this.stok,
      this.namaBarang,
      this.qty,
      this.harga,
      this.gambar,
      this.promo,
      this.deskripsi,
      this.hargaPromo,
      this.kategori});

  ValuesListBarang.fromJson(Map<String, dynamic> json) {
    idBarang = json['id_barang'];
    namaBarang = json['nama_barang'];
    qty = json['qty'];
    harga = json['harga'];
    gambar = json['gambar'];
    promo = json['promo'];
    deskripsi = json['deskripsi'];
    hargaPromo = json['harga_promo'];
    kategori = json['kategori'];
    stok=json['stok'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_barang'] = this.idBarang;
    data['nama_barang'] = this.namaBarang;
    data['qty'] = this.qty;
    data['harga'] = this.harga;
    data['gambar'] = this.gambar;
    data['promo'] = this.promo;
    data['deskripsi'] = this.deskripsi;
    data['harga_promo'] = this.hargaPromo;
    data['kategori'] = this.kategori;
    data['stok'] = this.stok;
    return data;
  }
}