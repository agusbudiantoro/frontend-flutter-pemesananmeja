class ModelListJadwal {
  int id;
  int idTim;
  String namaTim;
  String noTelp;
  String tanggalPemesanan;
  int idMeja;
  String paketWaktu;
  String nomorMeja;
  String jam;

  ModelListJadwal(
      {this.id,
      this.idTim,
      this.namaTim,
      this.noTelp,
      this.tanggalPemesanan,
      this.idMeja,
      this.paketWaktu,
      this.jam,
      this.nomorMeja});

  ModelListJadwal.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idTim = json['id_tim'];
    jam = json['jam'];
    namaTim = json['nama_tim'];
    noTelp = json['no_telp'];
    tanggalPemesanan = json['tanggal_pemesanan'];
    idMeja = json['id_meja'];
    paketWaktu = json['paket_waktu'];
    nomorMeja = json['nomor_meja'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['jam'] = this.jam;
    data['id_tim'] = this.idTim;
    data['nama_tim'] = this.namaTim;
    data['no_telp'] = this.noTelp;
    data['tanggal_pemesanan'] = this.tanggalPemesanan;
    data['id_meja'] = this.idMeja;
    data['paket_waktu'] = this.paketWaktu;
    data['nomor_meja'] = this.nomorMeja;
    return data;
  }
}