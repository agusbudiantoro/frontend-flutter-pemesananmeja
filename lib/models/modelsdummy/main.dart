class Product {
  final String name;
  final String imagePath;
  final int price;
  Product({this.imagePath, this.name, this.price});
}