class ModelGetPemesanan {
  List<ValueGetPemesanan> value;

  ModelGetPemesanan({this.value});

  ModelGetPemesanan.fromJson(Map<String, dynamic> json) {
    if (json['value'] != null) {
      value = new List<ValueGetPemesanan>();
      json['value'].forEach((v) {
        value.add(new ValueGetPemesanan.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.value != null) {
      data['value'] = this.value.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValueGetPemesanan {
  int noRiwayatPesanan;
  int idKonsumen;
  String waktuPengiriman;
  String namaPenerima;
  String alamat;
  String noHp;
  int idBarang;
  String namaBarang;
  int qty;
  int harga;
  String waktuPemesanan;
  int metodePembayaran;
  String estimasiSampai;
  int midtransIdTransaksi;
  String midtransIdPemesanan;
  int totalHarga;
  String image;
  String statusCodePembayaran;
  String status;
  String waktuPembayaran;
  String namaMetodePembayaran;
  String logoMetodePembayaran;
  int statusKirim;
  int statusPengiriman;
  String link_pembayaran;

  ValueGetPemesanan(
      {this.noRiwayatPesanan,
      this.idKonsumen,
      this.waktuPengiriman,
      this.namaPenerima,
      this.alamat,
      this.noHp,
      this.idBarang,
      this.namaBarang,
      this.qty,
      this.harga,
      this.waktuPemesanan,
      this.metodePembayaran,
      this.estimasiSampai,
      this.midtransIdTransaksi,
      this.midtransIdPemesanan,
      this.totalHarga,
      this.image,
      this.statusCodePembayaran,
      this.status,
      this.waktuPembayaran,
      this.logoMetodePembayaran,
      this.namaMetodePembayaran,
      this.statusKirim,
      this.link_pembayaran,
      this.statusPengiriman});

  ValueGetPemesanan.fromJson(Map<String, dynamic> json) {
    link_pembayaran= json['link_pembayaran'];
    statusKirim = json['status_kirim'];
    statusPengiriman = json['status_pengiriman'];
    namaMetodePembayaran = json['nama_metode_pembayaran'];
    logoMetodePembayaran = json['logo_metode_pembayaran'];
    waktuPembayaran = json['waktu_pembayaran'];
    noRiwayatPesanan = json['no_riwayat_pesanan'];
    idKonsumen = json['id_konsumen'];
    waktuPengiriman = json['waktu_pengiriman'];
    namaPenerima = json['nama_penerima'];
    alamat = json['alamat'];
    noHp = json['no_hp'];
    idBarang = json['id_barang'];
    namaBarang = json['nama_barang'];
    qty = json['qty'];
    harga = json['harga'];
    waktuPemesanan = json['waktu_pemesanan'];
    metodePembayaran = json['metode_pembayaran'];
    estimasiSampai = json['estimasi_sampai'];
    midtransIdTransaksi = json['midtrans_id_transaksi'];
    midtransIdPemesanan = json['midtrans_id_pemesanan'];
    totalHarga = json['total_harga'];
    image = json['image'];
    statusCodePembayaran = json['status_code_pembayaran'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status_kirim'] = this.statusKirim;
    data['status_pengiriman'] = this.statusPengiriman;
    data['logo_metode_pembayaran'] = this.logoMetodePembayaran;
    data['nama_metode_pembayaran'] = this.namaMetodePembayaran;
    data['waktu_pembayaran'] = this.waktuPembayaran;
    data['no_riwayat_pesanan'] = this.noRiwayatPesanan;
    data['id_konsumen'] = this.idKonsumen;
    data['waktu_pengiriman'] = this.waktuPengiriman;
    data['nama_penerima'] = this.namaPenerima;
    data['alamat'] = this.alamat;
    data['no_hp'] = this.noHp;
    data['id_barang'] = this.idBarang;
    data['nama_barang'] = this.namaBarang;
    data['qty'] = this.qty;
    data['link_pembayaran'] = this.link_pembayaran;
    data['harga'] = this.harga;
    data['waktu_pemesanan'] = this.waktuPemesanan;
    data['metode_pembayaran'] = this.metodePembayaran;
    data['estimasi_sampai'] = this.estimasiSampai;
    data['midtrans_id_transaksi'] = this.midtransIdTransaksi;
    data['midtrans_id_pemesanan'] = this.midtransIdPemesanan;
    data['total_harga'] = this.totalHarga;
    data['image'] = this.image;
    data['status_code_pembayaran'] = this.statusCodePembayaran;
    data['status'] = this.status;
    return data;
  }
}