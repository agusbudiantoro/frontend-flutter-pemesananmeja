class ModelListMejaTersedia {
  int idMeja;
  String nomorMeja;
  bool status;
  Data data;

  ModelListMejaTersedia({this.idMeja, this.nomorMeja, this.status, this.data});

  ModelListMejaTersedia.fromJson(Map<String, dynamic> json) {
    idMeja = json['id_meja'];
    nomorMeja = json['nomor_meja'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_meja'] = this.idMeja;
    data['nomor_meja'] = this.nomorMeja;
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  int idTim;
  String namaTim;

  Data({this.id, this.idTim, this.namaTim});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idTim = json['id_tim'];
    namaTim = json['nama_tim'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_tim'] = this.idTim;
    data['nama_tim'] = this.namaTim;
    return data;
  }
}